package com.senticap.controller;

import com.senticap.model.Event;
import com.senticap.view.BlurView;
import com.senticap.view.EventCreationDelegate;
import com.senticap.view.EventForm;
import com.senticap.view.EventFormDelegate;

import javax.swing.*;

import java.awt.*;


public class EventCreationController extends BlurView implements EventFormDelegate {
    
    /**
	 * This view is the event-creating view. 
	 * It will prompt you to enter the event name
	 * and some hashtags associated with the event
	 */
	private static final long serialVersionUID = 1L;
    private final EventForm eventForm;
    private EventCreationDelegate delegate;

    
    public EventCreationController(Event event) {
    	
    	super();
    	
    	SpringLayout layout = new SpringLayout();
    	this.setLayout(layout);
    	
    	
    	eventForm = new EventForm(event);
    	eventForm.setDelegate(this);
    	eventForm.setPreferredSize(new Dimension(350,300));
    	this.setConnectedComponent(eventForm);
    	
    	 layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, eventForm,
                 0,
                 SpringLayout.HORIZONTAL_CENTER, this);
    	 layout.putConstraint(SpringLayout.VERTICAL_CENTER, eventForm,
                 0,
                 SpringLayout.VERTICAL_CENTER, this);
    	
    	
    	 add(eventForm);
    	 

    }

	public EventCreationDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(EventCreationDelegate delegate) {
		this.delegate = delegate;
	}

	@Override
	public void saveButtonClicked(Object object) {
		
		this.delegate.eventCreationSuccessful((Event) object);	        
		this.fadeOut();

	}

	@Override
	public void cancelButtonClicked() {
		// TODO Auto-generated method stub
		
	}
    
   
}