package com.senticap.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SpringLayout;


public class MultipleViewController extends JPanel implements MultipleViewProtocol{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected ArrayList<JComponent> views;
	// a property that sets the index of the shown view 
	protected int shownViewIndex;
	protected SpringLayout viewContainerLayout;
	protected JPanel viewContainer;
	
	public MultipleViewController(){
		
		this.views = new ArrayList<JComponent>();
		
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		viewContainerLayout = new SpringLayout();
		
		viewContainer = new JPanel();
	    viewContainer.setBackground(new Color(255,0,0,255));
	    viewContainer.setPreferredSize(new Dimension(this.getWidth(),500));
	    viewContainer.setOpaque(true);
	    viewContainer.setLayout(viewContainerLayout);
	    viewContainer.setVisible(true);
			    
	    this.add(viewContainer);
		
	}
	
	public void addView(JComponent view){
		
		if(views.size() == 0) view.setVisible(true);
		else view.setVisible(false);
	
	    viewContainerLayout.putConstraint(SpringLayout.NORTH, view,
                0,
                SpringLayout.NORTH, viewContainer);
	    viewContainerLayout.putConstraint(SpringLayout.SOUTH, view,
                0,
                SpringLayout.SOUTH, viewContainer);
	    viewContainerLayout.putConstraint(SpringLayout.EAST, view,
                0,
                SpringLayout.EAST, viewContainer);
	    viewContainerLayout.putConstraint(SpringLayout.WEST, view,
                0,
                SpringLayout.WEST, viewContainer);
		
		views.add((JComponent)view);
		viewContainer.add(view);
		
	}
	
	public void selectionChanged(int index){
		
		shownViewIndex = index;
		
	}



	public int getShownViewIndex() {
		return shownViewIndex;
	}



	public void setShownViewIndex(int shownViewIndex) {
		this.shownViewIndex = shownViewIndex;
	}



	public ArrayList<JComponent> getViews() {
		return views;
	}



	public void setViews(ArrayList<JComponent> views) {
		this.views = views;
	}


	@Override
	public void selectionChanged(JComponent comp, int index) {
		
		JComponent oldView = views.get(shownViewIndex);
		oldView.setVisible(false);
		
		JComponent newView = views.get(index);
		newView.setVisible(true);
		
		setShownViewIndex(index);
		
	}
	
}
