package com.senticap.controller;

import java.awt.Color;

import javax.swing.plaf.FontUIResource;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.JFrame;
import javax.swing.SwingWorker;

import com.senticap.model.Globals;
import com.senticap.model.Event;
import com.senticap.model.GraphData;
import com.senticap.model.MySQLAccess;
import com.senticap.model.TweetStatus;
import com.senticap.view.CustomButton;
import com.senticap.view.DataView;
import com.senticap.view.EventCreationDelegate;
import com.senticap.view.GraphView;
import com.senticap.view.LoadingScreen;
import com.senticap.view.TextPanel;
import com.senticap.view.CustomTableRow;
import com.senticap.view.EventTableRow;
import com.senticap.view.CustomTableView;
import com.senticap.view.CustomTableViewDelegate;


public class MainWindow extends JFrame implements CustomTableViewDelegate, EventCreationDelegate{

		private static final long serialVersionUID = 1L;
		private ArrayList<Event> events;
		private CustomTableView eventScroller;
		private MainViewController mainView;
		private MainWindow self = this;
		private Container contentPane;
		
    //private static ScrollerUpdateWorker task;
		private EventCreationController newEventView;
		
		//private static ScrollerUpdateWorker task;
		/**
		 * Launch the application.
		 */
		public static void main(String[] args) {
			
			
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						MainWindow frame = new MainWindow();
						frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}

                    
				}
			});
		}

		/**
		 * Create the frame.
		 */
		public MainWindow(){

	        createGUI();
	    }
		
		private void createGUI(){
			
			// observers that will be notified when the selected
			// event changes. i.e. when the user clicks on a row
			// with the events
			
			ResourceBundle localizedStrings = Globals.getLocalizedStrings();
			
			//set UI-wide font
			Globals.setUIFont(new FontUIResource(new Font("Oxygen-Regular",Font.PLAIN, 15)));
			
	        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	       	        
	        contentPane = getContentPane();
	        SpringLayout layout = new SpringLayout();
	        contentPane.setLayout(layout);
	        setTitle("Senticap");
	        setMinimumSize(new Dimension(850,600));
	        
	        contentPane.setBackground(new Color(255,255,255,255));
	        
	        // this is the events table, where the user
	        // can choose between the events
	        eventScroller = new CustomTableView();
	        
	        
	        // the model for the events that need to be displayed
	        events = new ArrayList<Event>();

	        eventScroller.setBackground(new Color(19,24,30,255));
	        eventScroller.setDelegate(this);	    
	        eventScroller.displayRows();
	        eventScroller.getContentView().setBackground(new Color(19,24,30,255));
	        
	        int tableWidth = 250;
	        
	        //scroll view for the table
	        eventScroller.setPreferredSize(new Dimension(tableWidth,500));
	        
	        
	        mainView = new MainViewController();
	        // whenever event changes, notify observers
	        eventScroller.addObserver(mainView);
	        
	        DataViewController dataViewController = new DataViewController();
	        
	        GraphView graphView = new GraphView(new GraphData(null));
	        graphView.setPreferredSize(new Dimension(500,500));
	        
	        DataView dataView = new DataView();
			dataViewController.addView(graphView,"graphs_icon.png");
			dataViewController.addView(dataView ,"info_icon.png");
	        eventScroller.addObserver(dataView);
	        eventScroller.addObserver(graphView);
	        
	        /*MapViewController mapView = new MapViewController();
	        eventScroller.addObserver(mapView);*/
	        
	        TweetsViewController tweetsView = new TweetsViewController();
	        eventScroller.addObserver(tweetsView);
	        
	        JPanel prefPane = new JPanel();
	        prefPane.setPreferredSize(new Dimension(100,40));
	        SpringLayout prefPaneLayout = new SpringLayout();
	        prefPane.setLayout(prefPaneLayout);
	        CustomButton preferencesButton = new CustomButton("pref_icon.png", null);
	        preferencesButton.addMouseListener(new MouseListener(){

				@Override
				public void mouseClicked(MouseEvent e) {
					
					self.showPreferencesView();
					
				}

				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
	        	
	        	
	        });
	        preferencesButton.setPreferredSize(new Dimension(30,30));
	        preferencesButton.setColor(new Color(0,0,0,0));
	        prefPaneLayout.putConstraint(SpringLayout.WEST, preferencesButton, 
	        		3, 
	        		SpringLayout.WEST, prefPane);
	        prefPaneLayout.putConstraint(SpringLayout.NORTH, preferencesButton, 
	        		0, 
	        		SpringLayout.NORTH, prefPane);
	        prefPaneLayout.putConstraint(SpringLayout.SOUTH, preferencesButton, 
	        		0, 
	        		SpringLayout.SOUTH, prefPane);
	        
	        prefPane.setBackground(new Color(19,24,30,255));
	        prefPane.add(preferencesButton);
	        
	        mainView.addView(dataViewController, localizedStrings.getString("data"));
	        //mainView.addView(mapView, localizedStrings.getString("map"));
	        mainView.addView(tweetsView, localizedStrings.getString("tweets"));
	        
	        JLabel senticap_logo = new JLabel(new ImageIcon(Globals.prependBasePath(Globals.FOLDER.kImageFolder, "senticap_logo.png")));
	        
	        prefPaneLayout.putConstraint(SpringLayout.WEST, senticap_logo, 0, SpringLayout.EAST, preferencesButton);
	        prefPaneLayout.putConstraint(SpringLayout.EAST, senticap_logo, 0, SpringLayout.EAST, prefPane);
	        prefPaneLayout.putConstraint(SpringLayout.NORTH, senticap_logo, 0, SpringLayout.NORTH, prefPane);
	        prefPaneLayout.putConstraint(SpringLayout.SOUTH, senticap_logo, -8, SpringLayout.SOUTH, prefPane);
	        prefPane.add(senticap_logo);
	        
	        layout.putConstraint(SpringLayout.WEST, mainView,
                    0,
                    SpringLayout.EAST, eventScroller);
	        layout.putConstraint(SpringLayout.NORTH, mainView,
                    50,
                    SpringLayout.NORTH, contentPane);
	       
	        layout.putConstraint(SpringLayout.EAST, contentPane,
                    0,
                    SpringLayout.EAST, mainView);
	        layout.putConstraint(SpringLayout.SOUTH, contentPane,
                    40,
                    SpringLayout.SOUTH, eventScroller);
	        layout.putConstraint(SpringLayout.EAST, eventScroller,
	        		tableWidth,
                    SpringLayout.WEST, contentPane);
	        layout.putConstraint(SpringLayout.SOUTH, mainView,
                    0,
                    SpringLayout.SOUTH, contentPane);
	        
	        layout.putConstraint(SpringLayout.NORTH, prefPane,
                    -40,
                    SpringLayout.SOUTH, contentPane);
	        layout.putConstraint(SpringLayout.SOUTH, prefPane,
                    0,
                    SpringLayout.SOUTH, contentPane);
	        layout.putConstraint(SpringLayout.WEST, prefPane,
                    0,
                    SpringLayout.WEST, contentPane);
	        layout.putConstraint(SpringLayout.EAST, prefPane,
                    0,
                    SpringLayout.EAST, eventScroller);
	        
			contentPane.add(eventScroller);
			contentPane.add(mainView);
			contentPane.add(prefPane);
	        setVisible(true);
	        
	        loadEvents();
			
		}
		
		public void redraw(){
			
			contentPane.removeAll();
			this.remove(contentPane);
			createGUI();
			this.invalidate();
			this.validate();
			this.repaint();
			
		}

		@Override
		public CustomTableRow rowForObjectAtIndex(int index) {
			
			
			
			if(index == 0){				
				TextPanel addPanel = new TextPanel("+ "+Globals.getLocalizedStrings().getString("add_event"),
												   new Color(255,255,255,255),
												   new Color(18,28,45,255),
											 	   new Color(22,44,60,255));	
				addPanel.setSelectable(false);
				return addPanel;
			}
			
			//we subtract 1 because of add panel
			index -= 1;
			
			EventTableRow p = new EventTableRow(events.get(index),
							  new Color(27,36,50,255),
							  new Color(32,54,70,255));
			p.setData(events.get(index));
			return p;

		}

		@Override
		public int numberOfRowsInTable() {
			//we add 1 because of add panel
			return this.events.size()+1;
		}

		@Override
		public int widthForRowAtIndex(int index) {
			return 250;
		}

		@Override
		public int heightForRowAtIndex(int index) {
			if(index == 0) return 85;
			return 100;
		}
		
		@Override
		public void didSelectRowAtIndex(int panelIndex) {
			
			// this will be executed when the "+ Add Event" 
			// button is clicked
			if(panelIndex==0)showEventCreationView(null);			
			this.eventScroller.notifyObservers(events.get(panelIndex-1));
			
		}

		@Override
		public void didDeleteRowAtIndex(int index) {
			
			final Event evt = new Event(events.get(index-1)); 
			new SwingWorker<List<TweetStatus>, Void>(){
				
				private MySQLAccess mysql;
				@Override
				protected List<TweetStatus> doInBackground() throws Exception {
					
					mysql = new MySQLAccess();
					try {
						mysql.connectToBase();
						mysql.deleteEvent(evt);
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return null;
				}
				
				protected void done(){
					
					try {
						mysql.closeEverything();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
			}.execute();
			
			events.remove(index-1);
			eventScroller.displayRows();
			
		}

		@Override
		public void didEditRowAtIndex(int index) {
		
			showEventCreationView(events.get(index-1));
			
		}

		private void loadEvents(){
			
			this.showLoadingScreen();
			
			new SwingWorker<ArrayList<Event>, Event>(){
				
				private MySQLAccess mysql;
				@Override
				protected ArrayList<Event> doInBackground() throws Exception {
					
					ArrayList<Event> events = null;
					mysql = new MySQLAccess();
					try {
						mysql.connectToBase();
						events = mysql.getAllEvents();
						
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					return events;
				}
				
				protected void done(){
					
					try {
						try {
							self.events = get();
							LoadingScreen loadingScreen = LoadingScreen.getSharedInstance();
							loadingScreen.fadeOut();
							self.eventScroller.displayRows();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						mysql.closeEverything();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
			}.execute();
	
		}
		
		private void showEventCreationView(Event event){
	
			//shows the event creation view
			this.newEventView = new EventCreationController(event);
			this.newEventView.setDelegate(this);
			
			this.newEventView.setBounds(0,0,getWidth(),getHeight());
			this.newEventView.setPreferredSize(new Dimension(getWidth(),getHeight()));
			
			((SpringLayout)getContentPane().getLayout()).putConstraint(SpringLayout.NORTH, this.newEventView,
	        		0,
                    SpringLayout.NORTH, getContentPane());
			((SpringLayout)getContentPane().getLayout()).putConstraint(SpringLayout.WEST, this.newEventView,
	        		0,
                    SpringLayout.WEST, getContentPane());
			
			
			contentPane.add(this.newEventView);
			this.setGlassPane(this.newEventView);
			//this HAS TO BE after setGlassPane() !!!
			this.newEventView.fadeIn();
			
		}
		
		public void showLoadingScreen(){
			
			//shows the event creation view
			
			LoadingScreen loadingScreen = LoadingScreen.getSharedInstance();
			
			
			loadingScreen.setBounds(0,0,getWidth(),getHeight());
			loadingScreen.setPreferredSize(new Dimension(getWidth(),getHeight()));
			
			((SpringLayout)getContentPane().getLayout()).putConstraint(SpringLayout.NORTH, loadingScreen,
	        		0,
                    SpringLayout.NORTH, getContentPane());
			((SpringLayout)getContentPane().getLayout()).putConstraint(SpringLayout.WEST, loadingScreen,
	        		0,
                    SpringLayout.WEST, getContentPane());
			
			
			contentPane.add(loadingScreen);
			this.setGlassPane(loadingScreen);
			//this HAS TO BE after setGlassPane() !!!
			loadingScreen.fadeIn();
			
		}
		
		private void showPreferencesView(){
			
			//shows the event creation view
			
			PreferencesController preferences = new PreferencesController();
			
			
			preferences.setBounds(0,0,getWidth(),getHeight());
			preferences.setPreferredSize(new Dimension(getWidth(),getHeight()));
			
			((SpringLayout)getContentPane().getLayout()).putConstraint(SpringLayout.NORTH, preferences,
	        		0,
                    SpringLayout.NORTH, getContentPane());
			((SpringLayout)getContentPane().getLayout()).putConstraint(SpringLayout.WEST, preferences,
	        		0,
                    SpringLayout.WEST, getContentPane());
			
			
			contentPane.add(preferences);
			this.setGlassPane(preferences);
			//this HAS TO BE after setGlassPane() !!!
			preferences.fadeIn();
			
		}
		
		public void closeEventView(){
			
			this.remove(this.newEventView);
			this.repaint();
			
		}

		@Override
		public void eventCreationSuccessful(Event event) {
			
			final Event evt = event;
			if(event.getID() == -1){
				
				new SwingWorker<List<TweetStatus>, Void>(){
						
					@Override
					protected List<TweetStatus> doInBackground() throws Exception {
						
						MySQLAccess mysql = new MySQLAccess();
						try {
							mysql.connectToBase();
							int eventID = mysql.addEvent(evt.getName(), evt.getHashtagsString(","));
							evt.setID(eventID);
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return null;
					}
					
					
					
				}.execute();
				
				this.events.add(0,event);
			}
			else{
				
				new SwingWorker<List<TweetStatus>, Void>(){
					
					@Override
					protected List<TweetStatus> doInBackground() throws Exception {
						
						MySQLAccess mysql = new MySQLAccess();
						try {
							mysql.connectToBase();
							mysql.updateEvent(evt);
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return null;
					}
					
					
					
				}.execute();
				
			}
			
			this.eventScroller.displayRows();
        }

}


