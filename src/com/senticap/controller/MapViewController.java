package com.senticap.controller;

import javax.swing.JPanel;
import javax.swing.SpringLayout;
import com.senticap.model.Event;
import com.senticap.model.Observable;
import com.senticap.model.Observer;
import com.senticap.view.WorldMapView;



public class MapViewController extends JPanel implements Observer{
	
	/**
	 * 
	 * This is the map view 
	 * This view will show an svg map with 
	 * different coloring according to how 
	 * popular the current event is in a given country
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Event event;
	private WorldMapView worldMap;
	
	public MapViewController(){
	
			
			SpringLayout layout = new SpringLayout();
			setLayout(layout);
		    //add world map
	        worldMap = new WorldMapView();
	 
	        layout.putConstraint(SpringLayout.NORTH, worldMap,
                    0,
                    SpringLayout.NORTH, this);
		    layout.putConstraint(SpringLayout.SOUTH, worldMap,
                    0,
                    SpringLayout.SOUTH, this);
		    layout.putConstraint(SpringLayout.EAST, worldMap,
                    0,
                    SpringLayout.EAST, this);
		    layout.putConstraint(SpringLayout.WEST, worldMap,
                    0,
                    SpringLayout.WEST, this);
	        
	       
	        this.add(worldMap); 
	        setVisible(true);
	
	}


	public Event getEvent() {
		return event;
	}


	public void setEvent(Event event) {
		
		this.event = event;		
		
	}


	@Override
	public void update(Observable o, Object obj) {
		
		//Event event = (Event)obj;
		//worldMap.setEvent(event);
		
	}

}
