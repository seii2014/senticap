package com.senticap.controller;

import javax.swing.JComponent;

public interface MultipleViewProtocol {

	public void selectionChanged(JComponent comp, int index);
	
}
