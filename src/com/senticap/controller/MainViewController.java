package com.senticap.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

import com.senticap.model.Event;
import com.senticap.model.Observable;
import com.senticap.model.Observer;
import com.senticap.view.HorizontalButtonGroup;
import com.senticap.view.RoundedButton;

public class MainViewController extends MultipleViewController implements Observer{
	
	/**
	 * This is the main view. It is the view on the right hand side
	 * which has the event label at the top, the hashtag label beneath it
	 * the buttongroup underneath these two
	 * and the toggleable views controlled by the button group
	 */
	private static final long serialVersionUID = 1L;
	private JLabel eventLabel;
	private JLabel tagsLabel;
	private HorizontalButtonGroup buttonGroup;
	
	public MainViewController(){
		
		super();
		
		Font boldFont;
	    Font regularFont;
	    SpringLayout layout = new SpringLayout();
	    setLayout(layout);
	    setPreferredSize(new Dimension(500,500));
	    setBackground(new Color(255,255,255,255));
	    
	    try {
	    	  boldFont = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Bold.ttf")).deriveFont(30f);
	    	  regularFont = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Regular.ttf")).deriveFont(16f);
	   	} catch (Exception e) {
	    	  boldFont = new Font("Arial", Font.PLAIN, 16);
	    	  regularFont = new Font("Arial", Font.PLAIN, 16);
	    }
		  
	    eventLabel = new JLabel("-");
	    eventLabel.setBackground(new Color(255,255,255,255));
	    eventLabel.setPreferredSize(new Dimension(this.getWidth(),35));
	    eventLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    eventLabel.setFont(boldFont);
	    eventLabel.setForeground(new Color(60,60,60,255));
	    eventLabel.setOpaque(true);

	    tagsLabel = new JLabel("-");
	    tagsLabel.setBackground(new Color(255,255,255,255));
	    tagsLabel.setPreferredSize(new Dimension(this.getWidth(),40));
	    tagsLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    tagsLabel.setFont(regularFont);
	    tagsLabel.setForeground(new Color(90,90,90,255));
	    tagsLabel.setOpaque(true);
	    
	    buttonGroup = new HorizontalButtonGroup();
	    buttonGroup.setDelegate(this);
	   
	    
	    layout.putConstraint(SpringLayout.WEST, eventLabel,
                0,
                SpringLayout.WEST, this);
	    layout.putConstraint(SpringLayout.NORTH, eventLabel,
                0,
                SpringLayout.NORTH, this);
	    layout.putConstraint(SpringLayout.EAST, eventLabel,
                0,
                SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, eventLabel,
                0,
                SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, buttonGroup,
                0,
                SpringLayout.SOUTH, tagsLabel);
        // For Horizontal Alignment    
        layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, buttonGroup, 
        		0, 
        		SpringLayout.HORIZONTAL_CENTER, this);
        layout.putConstraint(SpringLayout.NORTH, tagsLabel,
                0,
                SpringLayout.SOUTH, eventLabel);
        layout.putConstraint(SpringLayout.WEST, tagsLabel,
                0,
                SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, tagsLabel,
                0,
                SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.NORTH, tagsLabel,
                0,
                SpringLayout.SOUTH, eventLabel);
        
        layout.putConstraint(SpringLayout.NORTH, viewContainer,
                0,
                SpringLayout.SOUTH, buttonGroup);
	    layout.putConstraint(SpringLayout.SOUTH, viewContainer,
                0,
                SpringLayout.SOUTH, this);
	    layout.putConstraint(SpringLayout.EAST, viewContainer,
                0,
                SpringLayout.EAST, this);
	    layout.putConstraint(SpringLayout.WEST, viewContainer,
                0,
                SpringLayout.WEST, this);
 
	    
        this.add(eventLabel);
        this.add(tagsLabel);
        this.add(buttonGroup);
        
        
	}
	
	public void addView(JComponent view, String buttonTitle){
		
		super.addView(view);
		//add button for view to button group
	    buttonGroup.addButton(new RoundedButton(buttonTitle));
	    if(buttonGroup.getButtons().size() == 1)
	    	buttonGroup.setSelectedIndex(0);
	    buttonGroup.display();
	}

	@Override
	public void update(Observable o, Object obj) {
		
		if(obj != null){
			Event event = (Event)obj;
			eventLabel.setText(event.getName());
			tagsLabel.setText(event.getHashtagsString(" "));
		}
	}

	
}
