package com.senticap.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.Locale;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;
import com.senticap.model.Globals;
import com.senticap.view.BlurView;
import com.senticap.view.CustomButton;
import com.senticap.view.CustomTableRow;
import com.senticap.view.CustomTableView;
import com.senticap.view.CustomTableViewDelegate;
import com.senticap.view.PreferenceTableRow;
import com.senticap.view.PreferencesContainer;

public class PreferencesController extends BlurView implements CustomTableViewDelegate{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomTableView preferencesTable;
	private PreferencesContainer preferencesContainer;
	private String[] preferenceStrings;
	private PreferencesController self = this;
	
	public PreferencesController(){
		
		SpringLayout layout = new SpringLayout();
		SpringLayout prefContainerLayout = new SpringLayout();
		setLayout(layout);
		
		this.preferenceStrings = new String[]{Globals.getLocalizedStrings().getString("language"),
							};
		
		
		preferencesContainer = new PreferencesContainer();
		preferencesContainer.setPreferredSize(new Dimension(400,500));
		preferencesContainer.setLayout(prefContainerLayout);
    	 layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, preferencesContainer,
                 0,
                 SpringLayout.HORIZONTAL_CENTER, this);
    	 layout.putConstraint(SpringLayout.VERTICAL_CENTER, preferencesContainer,
                 0,
                 SpringLayout.VERTICAL_CENTER, this);
    	
    	
    	 add(preferencesContainer);
		
    	 Font boldFont;
 	   
 	    try {
 	    	  boldFont = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Bold.ttf")).deriveFont(30f);
 	    	  //regularFont = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Regular.ttf")).deriveFont(16f);
 	   	} catch (Exception e) {
 	    	  boldFont = new Font("Arial", Font.PLAIN, 16);
 	    	  //regularFont = new Font("Arial", Font.PLAIN, 16);
 	    }
    	 
		JLabel titleLabel = new JLabel(Globals.getLocalizedStrings().getString("preferences"));
		titleLabel.setHorizontalAlignment(JLabel.CENTER);
		titleLabel.setForeground(new Color(60,60,60,255));
		titleLabel.setFont(boldFont);
		
		prefContainerLayout.putConstraint(SpringLayout.NORTH, titleLabel, 
				20, 
				SpringLayout.NORTH, preferencesContainer);
		prefContainerLayout.putConstraint(SpringLayout.WEST, titleLabel, 
				0, 
				SpringLayout.WEST, preferencesContainer);
		prefContainerLayout.putConstraint(SpringLayout.EAST, titleLabel, 
				0, 
				SpringLayout.EAST, preferencesContainer);
		
		this.setConnectedComponent(preferencesContainer);
		
		preferencesTable = new CustomTableView();
		preferencesTable.setDelegate(this);
		preferencesTable.setDelegate(this);
		preferencesTable.displayRows();		
		preferencesTable.setBackground(new Color(0,0,0,0));
		preferencesTable.setOpaque(false);
		preferencesTable.getViewport().setOpaque(false);
		preferencesTable.getViewport().setBackground(new Color(0,0,0,0));
		preferencesTable.getContentView().setOpaque(false);
		preferencesTable.getContentView().setBackground(new Color(0,0,0,0));
		preferencesTable.setOffset(2);
		
		prefContainerLayout.putConstraint(SpringLayout.NORTH, preferencesTable, 
				20, 
				SpringLayout.SOUTH, titleLabel);
		prefContainerLayout.putConstraint(SpringLayout.SOUTH, preferencesTable, 
				0, 
				SpringLayout.SOUTH, preferencesContainer);
		
		prefContainerLayout.putConstraint(SpringLayout.WEST, preferencesTable, 
				4, 
				SpringLayout.WEST, preferencesContainer);
		prefContainerLayout.putConstraint(SpringLayout.EAST, preferencesTable, 
				-4, 
				SpringLayout.EAST, preferencesContainer);
	
		preferencesContainer.add(preferencesTable);	
		preferencesContainer.add(titleLabel);
		
	}
	
	private void redrawFrame(){
		
		MainWindow frame = (MainWindow) SwingUtilities.getWindowAncestor(self);
		frame.redraw();
		
	}

	@Override
	public CustomTableRow rowForObjectAtIndex(int index) {
		
		
		PreferenceTableRow cell = new PreferenceTableRow(new Color(255,255,255,255),new Color(240,240,240,255));
		cell.setOpaque(false);
		cell.setSelectable(false);
		SpringLayout layout = new SpringLayout();
		cell.setLayout(layout);
		
		JLabel titleLabel = new JLabel();
		titleLabel.setForeground(new Color(140,140,140,255));
		titleLabel.setFont(titleLabel.getFont().deriveFont(20.0f));
		
		layout.putConstraint(SpringLayout.WEST, titleLabel, 
				10, 
				SpringLayout.WEST, cell);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, titleLabel, 
				0, 
				SpringLayout.VERTICAL_CENTER, cell);
		
		if(index == 0){
			
			CustomButton germanButton = new CustomButton("german.png", null);
			
			germanButton.addMouseListener(new MouseListener(){
				
				@Override
				public void mouseClicked(MouseEvent e) {
					Globals.setCurrentLocale(new Locale("de"));
					self.redrawFrame();
				}
				@Override
				public void mousePressed(MouseEvent e) {}
				@Override
				public void mouseReleased(MouseEvent e) {}
				@Override
				public void mouseEntered(MouseEvent e) {}
				@Override
				public void mouseExited(MouseEvent e) {}
	
	        });
			germanButton.setPreferredSize(new Dimension(30,30));
			germanButton.setColor(new Color(0,0,0,0));
			layout.putConstraint(SpringLayout.EAST, germanButton, 
	        		-5, 
	        		SpringLayout.EAST, cell);
	        layout.putConstraint(SpringLayout.NORTH, germanButton, 
	        		0, 
	        		SpringLayout.NORTH, cell);
	        layout.putConstraint(SpringLayout.SOUTH, germanButton, 
	        		0, 
	        		SpringLayout.SOUTH, cell);
	        
	        cell.add(germanButton);
	        
	        CustomButton englishButton = new CustomButton("english.png", null);
	        englishButton.addMouseListener(new MouseListener(){

				@Override
				public void mouseClicked(MouseEvent e) {
					Globals.setCurrentLocale(new Locale("en"));
					self.redrawFrame();
					
				}
				@Override
				public void mousePressed(MouseEvent e) {}
				@Override
				public void mouseReleased(MouseEvent e) {}
				@Override
				public void mouseEntered(MouseEvent e) {}
				@Override
				public void mouseExited(MouseEvent e) {}
	
	        });
	        englishButton.setPreferredSize(new Dimension(30,30));
	        englishButton.setColor(new Color(0,0,0,0));
			layout.putConstraint(SpringLayout.EAST, englishButton, 
	        		-45, 
	        		SpringLayout.EAST, cell);
	        layout.putConstraint(SpringLayout.NORTH, englishButton, 
	        		0, 
	        		SpringLayout.NORTH, cell);
	        layout.putConstraint(SpringLayout.SOUTH, englishButton, 
	        		0, 
	        		SpringLayout.SOUTH, cell);
	        
	        cell.setBackground(new Color(19,24,30,255));
	        cell.add(englishButton);
			
		}
		
		titleLabel.setText(this.preferenceStrings[index]);
		cell.add(titleLabel);

		return cell;
	}

	@Override
	public int numberOfRowsInTable() {
		// TODO Auto-generated method stub
		return this.preferenceStrings.length;
	}

	@Override
	public int widthForRowAtIndex(int index) {
		// TODO Auto-generated method stub
		return 393;
	}

	@Override
	public int heightForRowAtIndex(int index) {
		// TODO Auto-generated method stub
		return 60;
	}

	@Override
	public void didSelectRowAtIndex(int panelIndex) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void didDeleteRowAtIndex(int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void didEditRowAtIndex(int index) {
		// TODO Auto-generated method stub
		
	}
	
}
