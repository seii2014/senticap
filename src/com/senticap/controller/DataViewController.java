package com.senticap.controller;


import java.awt.Dimension;

import javax.swing.JComponent;
import javax.swing.SpringLayout;

import com.senticap.model.Observable;
import com.senticap.model.Observer;
import com.senticap.view.VerticalButtonMenu;

public class DataViewController extends MultipleViewController implements Observer,MultipleViewProtocol{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private VerticalButtonMenu menu;
	@Override
	public void update(Observable o, Object obj) {
		// TODO Auto-generated method stub
		
	}
	
	public DataViewController(){
		
		super();
		
		SpringLayout layout = new SpringLayout();
		setLayout(layout);

		menu = new VerticalButtonMenu();
		menu.setDelegate(this);
				
		setPreferredSize(new Dimension(500,500));
		
		layout.putConstraint(SpringLayout.WEST, menu,
				0,
				SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, menu,
				0,
				SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, menu,
				90,
				SpringLayout.WEST, menu);
		layout.putConstraint(SpringLayout.SOUTH, menu,
				0,
				SpringLayout.SOUTH, this);

		 layout.putConstraint(SpringLayout.NORTH, viewContainer,
	                0,
	                SpringLayout.NORTH, this);
		 layout.putConstraint(SpringLayout.SOUTH, viewContainer,
	                0,
	                SpringLayout.SOUTH, this);
		 layout.putConstraint(SpringLayout.EAST, viewContainer,
	                0,
	                SpringLayout.EAST, this);
		 layout.putConstraint(SpringLayout.WEST, viewContainer,
	                0,
	                SpringLayout.EAST, menu);
	
		add(menu);
		
	}
	
	public void addView(JComponent view, String iconRef){
		
		super.addView(view);
		menu.addButton(iconRef, null);
		
		
	}

}
