package com.senticap.controller;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Timer;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.senticap.model.DataBaseInsertionWorker;
import com.senticap.model.DataHandler;
import com.senticap.model.Event;
import com.senticap.model.FilterTweetsWorker;
import com.senticap.model.Globals;
import com.senticap.model.InitialTweetsLoaderWorker;
import com.senticap.model.LoadMoreTweetsWorker;
import com.senticap.model.Observable;
import com.senticap.model.Observer;
import com.senticap.model.TweetStatus;
import com.senticap.view.LoadingScreen;
import com.senticap.view.TextPanel;
import com.senticap.view.CustomTableRow;
import com.senticap.view.CustomTableView;
import com.senticap.view.CustomTableViewDelegate;
import com.senticap.view.CustomTextField;
import com.senticap.view.TweetTableRow;

public class TweetsViewController extends JPanel implements Observer,CustomTableViewDelegate,DataHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomTableView tweetsTable;
	private CustomTextField searchTextField;
	private ArrayList<TweetStatus> filteredTweets;
	private TweetsViewController component = this;
	private Timer inputTimer;
	private Event event;
	
	public TweetsViewController(){
		
		filteredTweets = new ArrayList<TweetStatus>();
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		searchTextField = new CustomTextField(Globals.getLocalizedStrings().getString("search"), new Color(0,0,0,0));
		searchTextField.addKeyListener(new KeyListener() {
	        
			public void keyPressed(KeyEvent e) {}
	        public void keyTyped(KeyEvent e) {}
	        
	        public void keyReleased(KeyEvent e) {
	        	
	        	
	        	if(component.inputTimer == null){
	        	
		        	component.inputTimer = new Timer(500, new ActionListener() {
			            @Override
			            public void actionPerformed(ActionEvent e) {
			            	
			            	new FilterTweetsWorker(component.event.getTweets(), searchTextField.getText(), component).execute();
			            	
			            }
	
	
			        });
			        component.inputTimer.start();
			        component.inputTimer.setRepeats(false);
	        	}else{
	        		
	        		component.inputTimer.restart();
	        		
	        	}
	        		        	 
	        }
	    });
		searchTextField.setPreferredSize(new Dimension(500,50));
	
		
		JPanel searchBarContainer = new JPanel();
		SpringLayout searchBarContainerLayout = new SpringLayout();
		searchBarContainer.setLayout(searchBarContainerLayout);
		searchBarContainer.setPreferredSize(new Dimension(400,50));
		searchBarContainer.setBackground(new Color(255,255,255,255));
		
		searchBarContainerLayout.putConstraint(SpringLayout.WEST, searchTextField,
				0,
				SpringLayout.WEST, searchBarContainer);
		searchBarContainerLayout.putConstraint(SpringLayout.NORTH, searchTextField,
				0,
				SpringLayout.NORTH, searchBarContainer);
		searchBarContainerLayout.putConstraint(SpringLayout.SOUTH, searchTextField,
				0,
				SpringLayout.SOUTH, searchBarContainer);
		
		tweetsTable = new CustomTableView();
		tweetsTable.setDelegate(this);
		
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, tweetsTable, 
				0, 
				SpringLayout.HORIZONTAL_CENTER, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, searchBarContainer, 
				0, 
				SpringLayout.HORIZONTAL_CENTER, this);
		
		layout.putConstraint(SpringLayout.NORTH, searchBarContainer, 
				10, 
				SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.NORTH, tweetsTable, 
				10, 
				SpringLayout.SOUTH, searchBarContainer);
		
		layout.putConstraint(SpringLayout.SOUTH, tweetsTable, 
				0, 
				SpringLayout.SOUTH, this);
		
		
	
		tweetsTable.setPreferredSize(new Dimension(600,200));
		searchBarContainer.setPreferredSize(new Dimension(600,50));
		tweetsTable.setDelegate(this);
		
		searchBarContainer.add(searchTextField);
		add(searchBarContainer);
		add(tweetsTable);
		
		
	}

	private List<TweetStatus> currentTweets(){
		
		if(searchTextField.getText().length() > 0){
			
			return filteredTweets;
			
		}
		
		return this.event.getTweets();
	}
	
	@Override
	public void update(Observable o, Object obj) {

		Event event = (Event) obj;
		this.event = event;
		// event has changed, load tweets for this 
		// particular event
		if(event != null)
		{
			MainWindow topFrame = (MainWindow) SwingUtilities.getWindowAncestor((Component) this);
	    	topFrame.showLoadingScreen();
			
			tweetsTable.clear();	
			// this call will also load more tweets that are
			// not in the database, after it finished loading those
			// from the db
			this.loadTweetsFromDB(event);
			
		}else{
			
			this.tweetsTable.clear();
			
		}
			
		
	}

	@Override
	public CustomTableRow rowForObjectAtIndex(int index) {
		
		if(index == 0){				
			TextPanel loadMoreButton = new TextPanel(Globals.getLocalizedStrings().getString("load_more"),
											   new Color(120,120,120,255),
											   new Color(250,250,250,255),
											   new Color(235,235,235,255));	
			loadMoreButton.getTitleLabel().setHorizontalAlignment( SwingConstants.CENTER );
			loadMoreButton.setSelectable(false);
			return loadMoreButton;
		}
		
		//we subtract 1 because of add panel
		index -= 1;
		
		TweetTableRow tweetRow = new TweetTableRow(currentTweets().get(index),
								 new Color(255,255,255,255),
								 new Color(250,250,250,255));
	
		return tweetRow;
	}

	@Override
	public int numberOfRowsInTable() {
		
		//+1 for add-panel
		return currentTweets().size() + 1;
	}

	@Override
	public int widthForRowAtIndex(int index) {
		return 600;
	}

	@Override
	public int heightForRowAtIndex(int index) {
		if(index == 0) return 80;
		return 160;
	}

	@Override
	public void didSelectRowAtIndex(int panelIndex) {
		
		if(panelIndex == 0 && this.event != null) 
			loadMoreTweets(this.event);
		else 
			System.out.println("Touched tweet");

		
	}
	
	private void loadTweetsFromDB(Event event){
		
		MainWindow topFrame = (MainWindow) SwingUtilities.getWindowAncestor((Component) this);
    	topFrame.showLoadingScreen();
		
		if(event != null){
			
			InitialTweetsLoaderWorker worker = new InitialTweetsLoaderWorker(this,event);
			worker.execute();
		}
		
	}
	
	private void loadMoreTweets(Event event){
		
		
		MainWindow topFrame = (MainWindow) SwingUtilities.getWindowAncestor((Component) this);
    	if(topFrame != null){
			topFrame.showLoadingScreen();
			
			if(event != null){
				long sinceID = 0;
				if(this.event.getTweets().size() > 0){
					TweetStatus firstTweet = this.event.getTweets().get(0);
					sinceID = firstTweet.getId();		
				}
				
				LoadMoreTweetsWorker worker = new LoadMoreTweetsWorker(this,event.getHashtags(),sinceID);
				worker.execute();
			}
    	}
		
	}
	
	@Override
	public void handleData(Object sender, Object obj) {
		
		Event event = this.event;
		
		//database tweets come in right here from bg thread!
		if(sender instanceof LoadMoreTweetsWorker)
		{
			System.out.println("load more!");
			if(obj instanceof List){
				
				@SuppressWarnings("unchecked")
				ArrayList<TweetStatus> statuses = (ArrayList<TweetStatus>)obj;
				System.out.println("loaded "+statuses.size()+"tweets.");
				event.insertTweets(0,statuses);
			}
			updateDBTweets();
		}
		else if(sender instanceof FilterTweetsWorker)
		{
			if(obj instanceof List){
				filteredTweets.clear();
				@SuppressWarnings("unchecked")
				List<TweetStatus> statuses = (List<TweetStatus>)obj;
				for(TweetStatus s : statuses)
					filteredTweets.add(0,s);
			
			}
		}else if(sender instanceof InitialTweetsLoaderWorker){
			
			System.out.println("initial load");
			if(obj instanceof List){
				
				@SuppressWarnings("unchecked")
				ArrayList<TweetStatus> statuses = (ArrayList<TweetStatus>)obj;
				
				event.insertTweets(0,statuses);
					
				
				loadMoreTweets(this.event);
	
			}
			
		}else if(sender instanceof DataBaseInsertionWorker){
			
			
			if(obj instanceof Boolean){
				
				if((Boolean)obj == true){
					
					System.out.println("insertion successful!");
					
				}else{
					
					System.out.println("insertion failed!");
					
				}
				
			}
			
		}
		
		
		tweetsTable.displayRows();
		LoadingScreen loadingScreen = LoadingScreen.getSharedInstance();
		loadingScreen.fadeOut();
		
	}
	
	private void updateDBTweets(){
		
		DataBaseInsertionWorker insertionWorker = new DataBaseInsertionWorker(this.event, this);
		insertionWorker.execute();
		
	}

	@Override
	public void didDeleteRowAtIndex(int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void didEditRowAtIndex(int index) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
