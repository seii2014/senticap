package com.senticap.controller;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import com.senticap.model.Timer;
import com.senticap.model.TweetsProvider;
import com.senticap.model.TweetsReader;
import twitter4j.Query;
import twitter4j.Status;
import twitter4j.TwitterException;

public class TemporaryTweetMain implements Observer {

	/**
	 * Total number of requests to the servers of Twitter
	 */
	private int requests = 1;
	/**
	 * number of current request
	 */
	private int j = 0;

	public static void temporary(String[] args) throws Exception {
		// Initialize Main class
		TemporaryTweetMain main = new TemporaryTweetMain();
		// get TweetsProvider
		TweetsProvider provider = TweetsReader.getInstance();
		// register Observer that is notified whenever a sample of Tweets is
		// available
		provider.addObserver(main);
		// initialize the TweetsProvider with the "login" data
		provider.init("lKdOnHjqpgdi8DCP7YJqDwf3g",
				"qxLpflQpztRuIQ3QclP49rceEw2JY9mtGZd7eCN2sXAV0kId0B",
				"2461834914-VkhoO0At8xISunPBDlopliBVwiTIuD6kROwD7ub",
				"AIXePxEvzoI3lR5c3HloItVIPBzlbQdeBhWILXASlwbKS");
		// initialize a query used to filter the incoming data stream
		Query query = new Query("@NeoGAF");
		// set language to filter
		query.setLang("en");
		// start loading the samples from the server...
		// hint: use null as input for loadSamples-method in order to access any Tweets
		// to know: Use Query query = new Query() without any configurations may
		// cause an exception!
		provider.loadSample(query);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		// state determines who (TweetsProvider or Timer-Thread) invoked that
		// method
		int state = (int) arg1;
		TweetsProvider provider = TweetsReader.getInstance();
		switch (state) {
		case TweetsReader.LOADED_TWEETS_AVAILABLE:
			// the Tweets are saved in the statuses array
			List<Status> statuses = provider.getSample();
			// do something
			int i = 0;
			while (statuses.get(i) != null) {
				Status tweet = statuses.get(i);
				if (tweet != null) {
					String userData = "Tweet ist null..";
					if (tweet.getUser() != null) {
						userData = "\nAuthor: "+ tweet.getUser().getName() +"\nPlace: "+ tweet.getUser().getLocation() +
								"\nLang: "+ tweet.getUser().getLang() +"\n";
					}
					System.out.println("TweetId: "+ tweet.getId() +"\nText: "+ tweet.getText() + userData);
				} else {
					System.out.println("Tweet ist Null..");
				}
				i++;
			}
			//Status tweet = statuses[0];
			/*if (tweet != null) {
				String userData = "Tweet ist null..";
				if (tweet.getUser() != null) {
					userData = "\nAuthor: "+ tweet.getUser().getName() +"\nPlace: "+ tweet.getUser().getLocation() +
							"\nLang: "+ tweet.getUser().getLang() +"\n";
				}
				System.out.println("TweetId: "+ tweet.getId() +"\nText: "+ tweet.getText() + userData);
			} else {
				System.out.println("Tweet ist Null..");
			}*/
			System.out.println("Number of Tweets in lap "+ j +": "+ i);
			if (++j < requests) {
				// call a new Timer Thread that will signal the update method
				// (flag WAITING_FINISHED), if further Tweets
				// can be obtained or rather the 15 minute (+ 2 safety margin!)
				// window is over.
				waitForMillis(17 * 60000);
			} else {
				// close data stream to Twitter..
				provider.close();
				System.exit(0);
			}
			break;
		case Timer.WAITING_FINISHED:
			try {
				// start loading another sample...
				// WARNING! Do not call the loadSample method before
				// WAITINIG_FINISHED has been signaled!
				// That may cause an exception, because there is a 15 minute
				// window to wait,
				Query query = new Query("NeoGAF");
				query.setLang("en");
				provider.loadSample(query);
			} catch (TwitterException e) {
				// TODO Auto-generated catch block
				System.out.println("Exception: " + e.getMessage());
				System.out.println("Cause: " + e.getCause());
				System.out.println("cause is rate limit exceeded: "
						+ e.exceededRateLimitation());
			}
			break;
		}
	}

	/**
	 * Calls a new instance of Timer Thread, which waits for specified time.
	 * 
	 * @param millis
	 *            time
	 */
	private void waitForMillis(int millis) {
		Timer timer = new Timer(millis, false);
		timer.addObserver(this);
		timer.start();
	}
	
	

}
