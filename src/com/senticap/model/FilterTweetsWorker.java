package com.senticap.model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class FilterTweetsWorker extends SwingWorker<List<TweetStatus>, Void> {

	private DataHandler handler;
	private List<TweetStatus> tweets;
	private String searchString;
	
	public FilterTweetsWorker(List<TweetStatus> tweets, String str, DataHandler handler){
		
		super();
		this.tweets = tweets;
		this.searchString = str;
		this.handler = handler;		
		
	}
	
	public List<TweetStatus> doInBackground() {
       
		ArrayList<TweetStatus> newTweets = new ArrayList<TweetStatus>(tweets.size()); 
		for(TweetStatus s : tweets)
		{
			if(s instanceof TweetStatus)
			{
				if((s.getText() != null && s.getText().contains(this.searchString))
				   || (s.getUser().getScreenName() != null && s.getUser().getScreenName().contains(this.searchString))
				   || (s.getUser().getName() != null && s.getUser().getName().contains(this.searchString)))
				{
					newTweets.add(s);
				}
				
			}
		}
		
		return newTweets;
    }

    @Override
    public void done() {
           	
    	try {
			handler.handleData(this,get());
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
	
}
