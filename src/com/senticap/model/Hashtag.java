package com.senticap.model;

public class Hashtag {

	private String name;

	public Hashtag(String name){
		
		this.name = name;
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		
		return this.name;
		
	}
	
}
