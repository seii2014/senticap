package com.senticap.model;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

public class ImageLoaderWorker extends SwingWorker<BufferedImage, BufferedImage> {

    private DataHandler handler;
    private TweetStatus tweet;

    public ImageLoaderWorker(DataHandler handler, TweetStatus tweet) {
        this.handler = handler;
        this.tweet = tweet;
    }

    @Override
    protected BufferedImage doInBackground() throws IOException {

        BufferedImage picture = ImageIO.read(new URL(this.tweet.getUser().getProfileImageURL()).openStream());
        return picture;

    }

    protected void done() {
        try {
            BufferedImage img = get();
            handler.handleData(this, img);
        } catch (Exception exp) {
            //exp.printStackTrace();
        }
    }           
}
