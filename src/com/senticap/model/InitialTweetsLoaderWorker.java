package com.senticap.model;

import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;

import com.senticap.view.LoadingScreen;

public class InitialTweetsLoaderWorker extends SwingWorker<List<TweetStatus>, TweetStatus> {

	private DataHandler handler;

	private Event event;
    private MySQLAccess db;
	
	public InitialTweetsLoaderWorker(DataHandler handler, Event event){
		
		super();
		
		this.db = new MySQLAccess();
		try {
			this.db.connectToBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.handler = handler;
		this.event = event;
		
	}
	
	public List<TweetStatus> doInBackground() {
       		
		List<TweetStatus> statuses = null;
		try {
			publish((TweetStatus)null);
			statuses = this.db.readDataBase(this.event.getID());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return statuses;
    }

	protected void process(List<TweetStatus> tweets){
		
		LoadingScreen.getSharedInstance().setLoadingText(Globals.getLocalizedStrings().getString("loadingfromdb"));
		
		
	}
	
    @Override
    public void done() {
         	
    	try {
    		try {
				this.db.closeEverything();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			handler.handleData(this,get());
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
	
}