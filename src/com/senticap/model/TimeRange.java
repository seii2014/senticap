package com.senticap.model;

public enum TimeRange {

	DAY,
	WEEK,
	MONTH,
	YEAR
			
}
