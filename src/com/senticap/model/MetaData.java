package com.senticap.model;

import java.util.ArrayList;

public class MetaData {
	
	public class MOOD{
		
		public final static int kMoodPositive = 0;
		public final static int kMoodNeutralNegative = 1;
		
	}
	
	public static float[] tweetMoodPercentages(ArrayList<TweetStatus> tweets){
		
		int totalCount = tweets.size();
		int positiveTweets = 0;
		int neutralnegativeTweets = 0;
		
		
		for(TweetStatus tweet : tweets){
			
			switch(tweet.getMood()){
				case SentimentClassifier.Sentiment.kPositiveMood:
					positiveTweets++;
					break;
				case SentimentClassifier.Sentiment.kNeutralMood:
					neutralnegativeTweets++;
					break;
				default: 
					break;
			}
			
		}
		
		float percentages[] = {((float)positiveTweets/(float)totalCount)*100.0f,
							   ((float)neutralnegativeTweets/(float)totalCount)*100.0f};
		
		return percentages;
		
	}
	
}
