package com.senticap.model;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;


public class Globals {
	
	public enum FOLDER{
		
		kImageFolder,
		kFontFolder
		
	};

	private static final String IMG_PATH = "resources/img/";
	private static final String FONT_PATH = "resources/font/";
	private static Locale currentLocale = Locale.getDefault();
	
	public static String prependBasePath(FOLDER f, String path){
		
		String folder = null;
		
		switch(f){
			case kImageFolder:
				folder = IMG_PATH;
				break;
			case kFontFolder:
				folder = FONT_PATH;
				break;		
		}
	
		return folder + path;
		
	}
	
	public static Locale getCurrentLocale(){
		
		return currentLocale;
		
	}
	
	public static void setCurrentLocale(Locale locale){
		
		currentLocale = locale;
		
	}
	
	public static ResourceBundle getLocalizedStrings(){
		
		return Globals.getLocalizedStrings(null);
	}
	
	public static ResourceBundle getLocalizedStrings(String lang){
		
		ResourceBundle localizedStrings;
		
		if(lang != null)
			currentLocale = new Locale(lang);
		else if(currentLocale == null)
			currentLocale = Locale.getDefault();
		
        localizedStrings = ResourceBundle.getBundle("Localization", currentLocale);
		return localizedStrings;
        
	}
	
	public static InputStream getPathForImage(String imgName){
		
		return Globals.class.getClassLoader().getResourceAsStream(Globals.prependBasePath(Globals.FOLDER.kImageFolder,imgName));
		
	}
	
	public static InputStream getPathForFont(String fontName){
		
		return Globals.class.getClassLoader().getResourceAsStream(Globals.prependBasePath(Globals.FOLDER.kFontFolder,fontName));
		
	}
	
	public static URL getImageURL(String imageName){
		
		return Globals.class.getClassLoader().getResource(Globals.prependBasePath(Globals.FOLDER.kImageFolder,imageName));
		
	}
	
	public static void setUIFont(FontUIResource f)
	{
	    Enumeration<Object> keys = UIManager.getDefaults().keys();
	    while (keys.hasMoreElements())
	    {
	        Object key = keys.nextElement();
	        Object value = UIManager.get(key);
	        if (value instanceof javax.swing.plaf.FontUIResource)
	        {
	            UIManager.put(key, f);
	        }
	    }
	}
	
	
}
