package com.senticap.model;


public interface Observable {
	
	public void notifyObservers(Object o);
	public void removeObserver(Observer o);
	public void addObserver(Observer o);
	
}
