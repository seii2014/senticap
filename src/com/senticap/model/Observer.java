package com.senticap.model;

public interface Observer {
	
	public void update(Observable o,Object obj);
	
}
