package com.senticap.model;

import twitter4j.*;

import java.util.Date;

/**
 * Created by Thomas Hagelmayer on 16.05.14.
 */

public class TweetStatus implements Status{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date date;
    private long id;
    private String text;
    private GeoLocation location;
    private User user;
    private String language;
    private int mood;
    
    public TweetStatus(Status status, int mood){
    	
    	this.date = status.getCreatedAt();
    	this.id = status.getId();
    	this.text = status.getText();
    	this.language = status.getIsoLanguageCode();
    	this.location = status.getGeoLocation();
    	this.user = status.getUser();
    	this.mood = mood;
   
    }
    
    public TweetStatus(){
    	
    	
    }
    
    public void setCreatedAt(Date date1) {
        this.date = date1;
    }

    @Override
    public Date getCreatedAt() {

        return this.date;
    }

    public void setId(long id1) {
        this.id = id1;
    }

    @Override
    public long getId() {
        return this.id;
    }

    public void setText(String text1) {
        this.text = text1;
    }

    @Override
    public String getText() {
        return this.text;
    }

    public void setGeoLocation(GeoLocation geoLocation) {
        this.location = geoLocation;
    }

    @Override
    public GeoLocation getGeoLocation() {
        return this.location;
    }

    public void setUser(User user1) {
        this.user = user1;
    }

    @Override
    public User getUser() {
        return this.user;
    }

    public void setIsoLanguageCode(String isoLanguageCode) {
        this.language = isoLanguageCode;
    }

    @Override
    public String getIsoLanguageCode() {
        return this.language;
    }

    public int getMood() {
        return mood;
    }

    public void setMood(int i){
        this.mood = i;
    }

    // everything after this line is not used

    @Override
    public String getSource() {
        return null;
    }

    @Override
    public boolean isTruncated() {
        return false;
    }

    @Override
    public long getInReplyToStatusId() {
        return 0;
    }

    @Override
    public long getInReplyToUserId() {
        return 0;
    }

    @Override
    public String getInReplyToScreenName() {
        return null;
    }

    @Override
    public Place getPlace() {
        return null;
    }

    @Override
    public boolean isFavorited() {
        return false;
    }

    @Override
    public boolean isRetweeted() {
        return false;
    }

    @Override
    public int getFavoriteCount() {
        return 0;
    }

    @Override
    public boolean isRetweet() {
        return false;
    }

    @Override
    public Status getRetweetedStatus() {
        return null;
    }

    @Override
    public long[] getContributors() {
        return new long[0];
    }

    @Override
    public int getRetweetCount() {
        return 0;
    }

    @Override
    public boolean isRetweetedByMe() {
        return false;
    }

    @Override
    public long getCurrentUserRetweetId() {
        return 0;
    }

    @Override
    public boolean isPossiblySensitive() {
        return false;
    }

    @Override
    public int compareTo(Status o) {
    	
        return this.getCreatedAt().compareTo(o.getCreatedAt());
    }

    @Override
    public UserMentionEntity[] getUserMentionEntities() {
        return new UserMentionEntity[0];
    }

    @Override
    public URLEntity[] getURLEntities() {
        return new URLEntity[0];
    }

    @Override
    public HashtagEntity[] getHashtagEntities() {
        return new HashtagEntity[0];
    }

    @Override
    public MediaEntity[] getMediaEntities() {
        return new MediaEntity[0];
    }

    @Override
    public SymbolEntity[] getSymbolEntities() {
        return new SymbolEntity[0];
    }

    @Override
    public RateLimitStatus getRateLimitStatus() {
        return null;
    }

    @Override
    public int getAccessLevel() {
        return 0;
    }
}
