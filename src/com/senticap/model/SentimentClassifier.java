package com.senticap.model;

import com.aliasi.classify.LMClassifier;
import com.aliasi.util.AbstractExternalizable;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class SentimentClassifier {

	public static class Sentiment{
		public static final int kPositiveMood = 1;
		public static final int kNeutralMood = 0;
		public static final int kNegativeMood = -1;
	}

	String[] categories;
	@SuppressWarnings("rawtypes")
	LMClassifier classifier;

	@SuppressWarnings("rawtypes")
	public SentimentClassifier() {
		NLP.init();
		try {
			URL url = getClass().getResource("classifier.txt");
			classifier = (LMClassifier) AbstractExternalizable.readObject(new File(url.getPath()));
			categories = classifier.categories();
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public String classify(String text) {


		int sentiment = NLP.findSentiment(text);

		if(sentiment > 1) {
			return "pos";
		} else if(sentiment < 1) {
			return "neg";
		}

		return "neu";

	}
}
