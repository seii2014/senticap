package com.senticap.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import com.senticap.view.LoadingScreen;

import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.conf.ConfigurationBuilder;

public class LoadMoreTweetsWorker extends SwingWorker<List<TweetStatus>, Integer> {

	private DataHandler handler;
	private ArrayList<Hashtag> hashtags;
	private TweetsHandler tweetshandler;
    private long sinceID;
    private SentimentClassifier sentimentClassifier;
	private int count = 0;
	private int progress = 0;
    
	public LoadMoreTweetsWorker(DataHandler handler, ArrayList<Hashtag> hashtags, long sinceID){
		
		super();
		this.tweetshandler = new TweetsHandler();
		this.handler = handler;
		this.hashtags = hashtags;
		this.sinceID = sinceID;
		this.sentimentClassifier = new SentimentClassifier();
		
	}
	
	public List<TweetStatus> doInBackground() {
       
		List<Status> statuses = null;
		ArrayList<TweetStatus> actualTweets = null;
		// The factory instance is re-useable and thread safe.
		ConfigurationBuilder cb = new ConfigurationBuilder();
	    cb.setDebugEnabled(true)
	            .setOAuthConsumerKey("lKdOnHjqpgdi8DCP7YJqDwf3g")
	            .setOAuthConsumerSecret("qxLpflQpztRuIQ3QclP49rceEw2JY9mtGZd7eCN2sXAV0kId0B")
	            .setOAuthAccessToken("2461834914-VkhoO0At8xISunPBDlopliBVwiTIuD6kROwD7ub")
	            .setOAuthAccessTokenSecret("AIXePxEvzoI3lR5c3HloItVIPBzlbQdeBhWILXASlwbKS");

	
        // querying the database while app is running
        try {        
        	
        	
   			statuses = tweetshandler.loadMoreTweets(hashtags,this.sinceID);
   			actualTweets = new ArrayList<TweetStatus>(statuses.size());
   				
   			count = statuses.size();
   			
   			for(Status s : statuses){
   				
   				progress++;
   				publish(0);
   				
   				String sentimentStr = sentimentClassifier.classify(s.getText());
   				int sentiment = 0;
   				switch(sentimentStr){
   				case "pos":
   					sentiment = SentimentClassifier.Sentiment.kPositiveMood;
   					break;
   				case "neu":
   					sentiment = SentimentClassifier.Sentiment.kNeutralMood;
   					break;
   				case "neg":
   					sentiment = SentimentClassifier.Sentiment.kNegativeMood;
   					break;
   				default: 
   					break;
   				}
   				
   				actualTweets.add(new TweetStatus(s,sentiment));
   			}
   			   			
        }catch (TwitterException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			
        } 
                
        return actualTweets;
    }

	protected void process(List<Integer> progress){
		
		LoadingScreen.getSharedInstance().setLoadingText(Globals.getLocalizedStrings().getString("loadingnewtweets")+" ("+(int)(((float)this.progress/(float)this.count)*100)+"%)");
	
	}
	
    @Override
    public void done() {
         	
		try {
			handler.handleData(this,get());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	
    }
	
}
