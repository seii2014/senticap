package com.senticap.model;

import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;

public class GraphData {
	
	private ArrayList<TweetStatus> tweets;
	private HashMap<Date, ArrayList<TweetStatus>> groupedTweets;
	

	public HashMap<Date, ArrayList<TweetStatus>> getGroupedTweets() {
		return groupedTweets;
	}

	public void setGroupedTweets(HashMap<Date, ArrayList<TweetStatus>> groupedTweets) {
		this.groupedTweets = groupedTweets;
	}

	public GraphData(ArrayList<TweetStatus> tweets){
		
		if(tweets != null)
			setTweets(tweets);
		
	}
	
	private void groupTweets(){
		
		groupedTweets = new HashMap<Date, ArrayList<TweetStatus>>();
		
		Date compareDate = null;
		ArrayList<TweetStatus> tweets = new ArrayList<TweetStatus>();
		for(TweetStatus tweet : this.tweets){
			
			if(compareDate != null){
				
				if(DateUtils.getZeroTimeDate(compareDate).equals(DateUtils.getZeroTimeDate(tweet.getCreatedAt())) 
					&& !(tweet == this.tweets.get(this.tweets.size()-1))){
					tweets.add(tweet);
					
				}else{
					groupedTweets.put(DateUtils.getZeroTimeDate(compareDate), new ArrayList<TweetStatus>(tweets));
					tweets.clear();
				}
			
			}
			
			compareDate = tweet.getCreatedAt();
			
		}
		
	}
	
	
	
	public ArrayList<TweetStatus> getTweets() {
		return tweets;
	}

	public void setTweets(ArrayList<TweetStatus> tweets) {
		this.tweets = tweets;
		groupTweets();
	}

}
