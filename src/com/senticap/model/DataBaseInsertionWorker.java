package com.senticap.model;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;

import com.senticap.view.LoadingScreen;

public class DataBaseInsertionWorker extends SwingWorker<Boolean, Integer> {

	private DataHandler handler;
	private MySQLAccess db;
	private Event event;
	
	public DataBaseInsertionWorker(Event event, DataHandler handler){
		
		super();
				
		this.handler = handler;
		
		//create copy to prevent concurrent changes on the same object
		this.event = new Event(event);
			
		this.db = new MySQLAccess();
		try {
			this.db.connectToBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
		
	}
	
	public Boolean doInBackground() {
       		
		int eventID = this.event.getID();
		for(TweetStatus s : this.event.getTweets()){
			try {
				this.db.insertIntoBase(
						eventID, 
						s.getId(), 
						s.getIsoLanguageCode(), 
						s.getUser().getLocation(), 
						new Timestamp(s.getCreatedAt().getTime()), 
						s.getMood(), 
						s.getText(), 
						s.getUser().getName(),
						s.getUser().getBiggerProfileImageURLHttps());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		return true;
    }

	protected void process(List<Integer> progress){
		
		LoadingScreen.getSharedInstance().setLoadingText((progress.get(progress.size()-1)+"%"));
		
		
	}
	
    @Override
    public void done() {
         	
    	try {
    		try {
				this.db.closeEverything();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			handler.handleData(this,get());
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
	
}