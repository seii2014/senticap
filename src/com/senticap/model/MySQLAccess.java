package com.senticap.model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLAccess {
    private Connection connect = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private List<TweetStatus> tweets = new ArrayList<TweetStatus>();
    private TweetStatus status;

    // establish connection to DB
    public void connectToBase() throws Exception {
        try {
            // load the MySQL driver
            Class.forName("com.mysql.jdbc.Driver");
            // setup the connection with the DB
            connect = DriverManager.getConnection(
                    "jdbc:mysql://54.244.225.71", "senticap_team",
                    "uni2014"); // url to hosting site, username, password
        } catch (Exception e) {
            System.out.println("Can not connect to database.");
            throw e;
        }
    }

    // close the connection to the DB
    public void closeEverything() throws SQLException {
        if (this.resultSet != null) {
            try {
                this.resultSet.close();
            } catch (SQLException e) {
                System.out.println("Cant close connection.");
            }
        }
        if (this.connect != null) {
            try {
                this.connect.close();
            } catch (SQLException e) {
                System.out.println("Cant close connection.");
            }
        }
    }

    /**
     *  methods for working with tweets
     */

    // use to insert something into the DB
    public void insertIntoBase(int eventID, long idTweet, String language, String location, Timestamp date, int mood, String tweet, String author, String picture) throws SQLException {
        
    	
    	try {
            // preparedStatement can use variables and are more efficient
            preparedStatement = connect
                    .prepareStatement("insert ignore into senticap.tweet values (?,?,?,?,?,?,?,?,?)");
            // values  "id of the tweet, language, location, date, mood, tweet, author"
            // parameters start with 1
            preparedStatement.setLong(1, idTweet);
            preparedStatement.setString(2, language);
            preparedStatement.setString(3, location);
            preparedStatement.setTimestamp(4, date);
            preparedStatement.setInt(5, mood);
            preparedStatement.setString(6, tweet);
            preparedStatement.setString(7, author);
            preparedStatement.setString(8, picture);
            preparedStatement.setInt(9, eventID);
            preparedStatement.executeUpdate();
       
            
        } catch (SQLException e) {
            System.out.println("Insert failed.");
            throw e;
        }
    	
    	
    }

    // get tweet records from the DB
    // use a String to specify event
    public List<TweetStatus> readDataBase(int eventID) throws SQLException {
        try {
            preparedStatement = connect.prepareStatement("select * from senticap.tweet WHERE event_id ="+eventID+";");
            resultSet = preparedStatement.executeQuery();
            tweets = writeResultSet(resultSet);
        } catch (SQLException e) {
            System.out.println("Read failed");
            throw e;
        }

        return tweets;
    }

    // print table data to console
    // change this
    private List<TweetStatus>  writeResultSet(ResultSet resultSet) throws SQLException {
        // resultSet is initialised before the first data set
        while (resultSet.next()) {
            status = new TweetStatus();
            // it is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g., resultSet.getString(2);

            Long id = resultSet.getLong("tweet_id");
            String language = resultSet.getString("language");
            String location = resultSet.getString("location");
            Date date = resultSet.getDate("date");
            int mood = resultSet.getInt("mood");
            String tweet = resultSet.getString("tweet");
            String author = resultSet.getString("author");
            String picture = resultSet.getString("picture");

            // convert Strings to the needed Format
            

            TwitterUser user = new TwitterUser();
            user.setScreenName(author);

            // add them to status
            status.setCreatedAt(date);
            user.setLocation(location);
            status.setId(id);
            status.setMood(mood);
            status.setIsoLanguageCode(language);
            status.setUser(user);
            status.setText(tweet);
            user.setProfileImageURL(picture);

            // add status to list
            tweets.add(status);
        }

        return tweets;
    }

    


    /**
     * methods for working with events
     */

    //  use this for saving events with their hashtags
    public int addEvent(String eventName, String hashtags) throws SQLException {
    	
    	int insertedEventID = -1;
    	
        try {
            preparedStatement = connect.prepareStatement("insert ignore into senticap.event values (?,?,?)");
            preparedStatement.setString(1, eventName);
            preparedStatement.setString(2, hashtags);
            preparedStatement.setInt(3, 0);
            preparedStatement.execute();
            
            ResultSet rs = preparedStatement.getGeneratedKeys();
        	if (rs.next()){
        	    insertedEventID=rs.getInt(1);
        	}
            
        } catch (SQLException e){
            System.out.println("Can not save event.");
            throw e;
        }
        
        return insertedEventID;
    }

    // read all Events from the database
    public ArrayList<Event> getAllEvents() throws SQLException {
        try {
            preparedStatement = connect.prepareStatement("select * from senticap.event order by event_id desc");
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            System.out.println("Getting events failed.");
            throw e;
        }

        ArrayList<Event> events = new ArrayList<Event>();
        
        while (resultSet.next()) {
            String eventName = resultSet.getString("eventName");
            String hashtags = resultSet.getString("hashtags");
            int event_id = resultSet.getInt("event_id");
            events.add(new Event(event_id,eventName, Event.getHashtagsFromString(hashtags, ",")));
        }
        return events;
    }

    // change Event name and hashtags
    public void updateEvent(Event event) throws SQLException{
        try{
          
            // update event with new values
            preparedStatement = connect.prepareStatement("update senticap.event set eventName =?, hashtags=? where event_id=?");
            preparedStatement.setString(1, event.getName());
            preparedStatement.setString(2, event.getHashtagsString(","));
            preparedStatement.setInt(3, event.getID());
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public void deleteEvent(Event event) throws SQLException{
        try{
          
            // update event with new values
            preparedStatement = connect.prepareStatement("delete from senticap.event where event_id=?");
            preparedStatement.setInt(1, event.getID());
            preparedStatement.executeUpdate();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Use something like this to get the necessary information from the list that is created in the method above
     *
     *
     *              ArrayList<String> eventString = mysql.getAllEvents();
     *              for (String string: eventString){
     *                  String [] events = string.split(",");
     *                  System.out.println("Eventname: " + events[0]  + "; Hashtags: " + events[1]);
     *              }
     *
     */
}