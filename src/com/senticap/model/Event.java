package com.senticap.model;

import java.util.ArrayList;

public class Event implements Observable{
	
	/**
	 * This is the model for an event
	 */
	
	private int ID;
	private String name;
	private ArrayList<Hashtag>hashtags;
	private ArrayList<TweetStatus>tweets;
	private ArrayList<Observer> observers;
	
	@Override
	public void notifyObservers(Object obj) {
		
		for(Observer o : this.observers){
			
			o.update(this, obj);
			
		}
		
	}


	@Override
	public void removeObserver(Observer o) {

		this.observers.remove(o);
		
	}

	@Override
	public void addObserver(Observer o) {
		
		if(!this.observers.contains(o))
			this.observers.add(o);
		
	}
	
	public Event(int ID, String name, ArrayList<Hashtag> hashtags){
		
		this.ID = ID;
		this.name = name;
		this.hashtags = hashtags;
		this.observers = new ArrayList<Observer>();
		
	}
	
	public Event(Event event){
		
		this.ID = event.ID;
		this.name = event.name;
		this.hashtags = event.hashtags;
		this.observers = event.observers;
		
	}
	
	public Event(){
		
	}
	
	public String getHashtagsString(String separator){
		
		String tagString = "";
		int count = 0;
		for(Hashtag h : this.hashtags){
			count++;
			String sep = count == this.hashtags.size() ? "" : separator; 
			tagString += (h+sep);
			
		}
		
		return tagString;
	}
	
	public static ArrayList<Hashtag> getHashtagsFromString(String hashtagString, String separator){
		
		String[] parts = hashtagString.split(separator);
		
		ArrayList<Hashtag> hashtags = new ArrayList<Hashtag>(parts.length);  
		for (String s : parts) {  
			hashtags.add(new Hashtag(s));  
		}  
		
		return hashtags;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Hashtag> getHashtags() {
		return hashtags;
	}

	public void setHashtags(ArrayList<Hashtag> hashtags) {
		this.hashtags = hashtags;
	}

	public ArrayList<TweetStatus> getTweets() {
		if(tweets == null)
			tweets = new ArrayList<TweetStatus>();
		return tweets;
	}

	public void setTweets(ArrayList<TweetStatus> tweets) {
		this.tweets = tweets;
		this.notifyObservers(this);
	}
	
	public void insertTweets(int index, ArrayList<TweetStatus> tweets){
		for(TweetStatus s : tweets){
			this.tweets.add(index, s);
		}
		notifyObservers(this);
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
	
	
}
