package com.senticap.model;

public interface DataHandler {
	
	public void handleData(Object sender, Object obj);
	
}
