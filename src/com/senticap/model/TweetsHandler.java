package com.senticap.model;

import twitter4j.Query;
import twitter4j.Status;
import twitter4j.TwitterException;

import java.util.List;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Provides methods for working with Tweets
 *
 */
public class TweetsHandler implements Observer {

	/**
	 * Total number of requests to the servers of Twitter
	 */
	//private int requests = 1;

	/**
	 * number of current request
	 */
	private int j = 0;

	TweetsProvider provider;
	SentimentClassifier sentClassifier;

	public TweetsHandler() {
		// get TweetsProvider
		provider = TweetsReader.getInstance();

		// register Observer that is notified whenever a sample of Tweets is
		// available
		provider.addObserver(this);

		// initialize the TweetsProvider with the "login" data
		provider.init("lKdOnHjqpgdi8DCP7YJqDwf3g",
				"qxLpflQpztRuIQ3QclP49rceEw2JY9mtGZd7eCN2sXAV0kId0B",
				"2461834914-VkhoO0At8xISunPBDlopliBVwiTIuD6kROwD7ub",
				"AIXePxEvzoI3lR5c3HloItVIPBzlbQdeBhWILXASlwbKS");

		sentClassifier = new SentimentClassifier();
	}

	public List<Status> loadMoreTweets(ArrayList<Hashtag> hashtags, long sinceID) throws TwitterException {
		String searchString = "";

		//Create correct String for the query
		for(int i = 0; i < hashtags.size(); i++) {
			if(i == hashtags.size() - 1) {
				searchString = searchString + hashtags.get(i);
			} else {
				searchString = searchString + hashtags.get(i) + " OR ";
			}
		}
		// initialize a query used to filter the incoming data stream
		Query query = new Query(searchString);
		// set language to filter
		//query.setLang(Globals.getCurrentLocale().getLanguage());
		if(sinceID != 0)
			query.setSinceId(sinceID);
		
		// start loading the samples from the server...
		// hint: use null as input for loadSamples-method in order to access any Tweets
		// to know: Use Query query = new Query() without any configurations may
		// cause an exception!
		List<Status>tweets = provider.loadSample(query);
		return tweets;
	}

	@Override
	public void update(Observable o, Object obj) {

		//List<Status> statuses = provider.getSample();
		//printData(statuses);
		
	}
	
	@SuppressWarnings("unused")
	private void printData(List<TweetStatus> statuses){
		
		// do something
				int i = 0;
				while (statuses.get(i) != null) {
					TweetStatus tweet = statuses.get(i);
					if (tweet != null) {
						String userData = "Tweet ist null..";
						if (tweet.getUser() != null) {
							userData = "\nAuthor: "+ tweet.getUser().getName() +"\nPlace: "+ tweet.getUser().getLocation() +
									"\nLang: "+ tweet.getUser().getLang() +"\nDate: " + tweet.getCreatedAt() +"\n";
							String sentiment = sentClassifier.classify(tweet.getText());
							System.out.println(sentiment);

							/*try {
								preparedStatement = connect.prepareStatement("insert ignore into senticap_senticap.MensUSOpen values (?, ?, ?, ?, ?, ?, ?)");
								preparedStatement.setLong(1, tweet.getId());
								preparedStatement.setString(2, tweet.getUser().getLang());
								preparedStatement.setString(3, tweet.getUser().getLocation());
								preparedStatement.setString(4, tweet.getCreatedAt().toString());
								preparedStatement.setInt(5, 0);
								preparedStatement.setString(6, tweet.getText());
								preparedStatement.setString(7, tweet.getUser().getScreenName());
								preparedStatement.executeUpdate();
							} catch (SQLException e) {
								e.printStackTrace();
							}*/
						}
						System.out.println("TweetId: "+ tweet.getId() +"\nText: "+ tweet.getText() + userData);

					} else {
						System.out.println("Tweet ist Null..");
					}
					i++;
					System.out.println("Number of Tweets in lap "+ j +": "+ i);
				}

		
	}
	
}
