package com.senticap.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

import com.senticap.model.DataHandler;
import com.senticap.model.ImageLoaderWorker;
import com.senticap.model.SentimentClassifier;
import com.senticap.model.TweetStatus;


public class TweetTableRow extends CustomTableRow implements DataHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel screenNameLabel;
	private JLabel atNameLabel;
	private JLabel tweetTextLabel;
	private TweetStatus status;
	private RoundImage profileImage;
	
	public TweetTableRow(TweetStatus status,Color primaryColor, Color selectedColor){
		
		super(primaryColor,selectedColor);
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		this.status = status;
		
		if(status.getUser().getProfileImageURL() != null){
			ImageLoaderWorker worker = new ImageLoaderWorker(this,status);
			worker.execute();
		}
		
		profileImage = new RoundImage(null);
		profileImage.setPreferredSize(new Dimension(60,60));
		
		//screenname 
		screenNameLabel = new JLabel(status.getUser().getName());
		screenNameLabel.setForeground(new Color(55,55,55,255));
		screenNameLabel.setFont(new Font("Oxygen-Regular", Font.PLAIN, 22));
				
		//@name label
		atNameLabel = new JLabel("@"+status.getUser().getScreenName());
		atNameLabel.setForeground(new Color(125,125,125,255));
		atNameLabel.setFont(new Font("Oxygen-Regular", Font.PLAIN, 14));
		
		//@name label
		String tweetText = String.format("<html><div style=\"width:%dpx;\">%s</div></html>", 360, status.getText());
		tweetTextLabel = new JLabel(tweetText);
		tweetTextLabel.setForeground(new Color(55,55,55,255));
		tweetTextLabel.setFont(new Font("Oxygen-Regular", Font.PLAIN, 16));
		
		
		
		CircleView sentimentCircle = new CircleView();
		switch(this.status.getMood()){
		case SentimentClassifier.Sentiment.kPositiveMood:
			sentimentCircle.setColor(new Color(0,235,30,255));
			break;
		case SentimentClassifier.Sentiment.kNeutralMood:
			sentimentCircle.setColor(new Color(255,150,0,255));
			break;
		case SentimentClassifier.Sentiment.kNegativeMood:
			sentimentCircle.setColor(new Color(255,0,0,255));
			break;
		
		}
		
		sentimentCircle.setPreferredSize(new Dimension(8,8));
		
		layout.putConstraint(SpringLayout.NORTH, sentimentCircle, 
				8, 
				SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, sentimentCircle, 
				-8, 
				SpringLayout.EAST, this);
		
		layout.putConstraint(SpringLayout.NORTH, profileImage, 
				20, 
				SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, profileImage, 
				20, 
				SpringLayout.WEST, this);
		
		layout.putConstraint(SpringLayout.NORTH, screenNameLabel, 
				0, 
				SpringLayout.NORTH, profileImage);
		layout.putConstraint(SpringLayout.WEST, screenNameLabel, 
				20, 
				SpringLayout.EAST, profileImage);
		
		layout.putConstraint(SpringLayout.NORTH, atNameLabel, 
				0, 
				SpringLayout.SOUTH, screenNameLabel);
		layout.putConstraint(SpringLayout.WEST, atNameLabel, 
				0, 
				SpringLayout.WEST, screenNameLabel);
		
		layout.putConstraint(SpringLayout.NORTH, tweetTextLabel, 
				5, 
				SpringLayout.SOUTH, atNameLabel);
		layout.putConstraint(SpringLayout.WEST, tweetTextLabel, 
				0, 
				SpringLayout.WEST, screenNameLabel);
		layout.putConstraint(SpringLayout.EAST, tweetTextLabel, 
				-20, 
				SpringLayout.EAST, this);
		
		add(screenNameLabel);
		add(atNameLabel);
		add(tweetTextLabel);
		add(sentimentCircle);
		add(profileImage);
		
	}

	public TweetStatus getStatus() {
		return status;
	}

	public void setStatus(TweetStatus status) {
		
		this.status = status;
		
		screenNameLabel.setText(status.getUser().getName());
		atNameLabel.setText(status.getUser().getScreenName());
	}

	public JLabel getAtNameLabel() {
		return atNameLabel;
	}

	public void setAtNameLabel(JLabel atNameLabel) {
		this.atNameLabel = atNameLabel;
	}

	public JLabel getScreenNameLabel() {
		return screenNameLabel;
	}

	public void setScreenNameLabel(JLabel screenNameLabel) {
		this.screenNameLabel = screenNameLabel;
	}

	public JLabel getTweetTextLabel() {
		return tweetTextLabel;
	}

	public void setTweetTextLabel(JLabel tweetTextLabel) {
		this.tweetTextLabel = tweetTextLabel;
	}

	@Override
	public void handleData(Object sender, Object obj) {
		
		if(sender instanceof ImageLoaderWorker){
			if(obj instanceof BufferedImage && obj != null)
				profileImage.setImage((BufferedImage)obj);
			
		}
		
	}
	

}
