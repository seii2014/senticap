package com.senticap.view;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.Border;
import com.senticap.model.Observable;
import com.senticap.model.Observer;

public class CustomTableView extends JScrollPane implements CustomTableRowDelegate,KeyListener,Observable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CustomTableViewDelegate delegate;
	private JPanel contentView;
	private int selectedIndex;
	private ArrayList<CustomTableRow> panels;
	private ArrayList<Observer> observers;
	private int offset;
	
	public CustomTableView()
    {
		
        super(VERTICAL_SCROLLBAR_AS_NEEDED,HORIZONTAL_SCROLLBAR_NEVER);

        
        contentView = new JPanel();
        contentView.setOpaque(true);
        this.setViewportView(contentView);
        this.panels = new ArrayList<CustomTableRow>();

        // add key listener for up/down events 
        this.addKeyListener(this);
        this.setFocusable(true);
        
        // make customizations to vertical scrollbar
        // faster scrolling, size and color changes
        getVerticalScrollBar().setUnitIncrement(16);
        getVerticalScrollBar().setPreferredSize(new Dimension(3,0));
        getVerticalScrollBar().setBackground(new Color(0,0,0,255));
        getVerticalScrollBar().setBorder(BorderFactory.createEmptyBorder());
        
        this.observers = new ArrayList<Observer>(); 
       
    }
	
	public void keyTyped(KeyEvent e) {
        //displayInfo(e, "KEY TYPED: ");
    }

    /** Handle the key-pressed event from the text field. */
    public void keyPressed(KeyEvent e) {
        displayInfo(e, "KEY PRESSED: ");
    }

    /** Handle the key-released event from the text field. */
    public void keyReleased(KeyEvent e) {
        //displayInfo(e, "KEY RELEASED: ");
    }
    
    private void displayInfo(KeyEvent e, String keyStatus){
        
        //You should only rely on the key char if the event
        //is a key typed event.
        int id = e.getID();
        int keyCode = -1;
        
        if (id == KeyEvent.KEY_TYPED) {
            //char c = e.getKeyChar();
            //keyString = "key character = '" + c + "'";
        } else {
            keyCode = e.getKeyCode();
        }
        
        //up arrow pressed
       	switch(keyCode){
       		
       		case 38:
       			setSelectedIndex(selectedIndex-1);
       			break;
       		case 40:
       			setSelectedIndex(selectedIndex+1);
       			break;
       		default:
       			break;
       	
       	}
        	
        
        //Display information about the KeyEvent...
    }
		
	public void setBorder(Border border){
		
		//overridden because we don't want a border
		
	}
	

	public void displayRows() {
		
		clear();
		
		int count = this.delegate.numberOfRowsInTable();
		
		for(int i=0;i<count;i++){
			
			int w = this.delegate.widthForRowAtIndex(i);
			int h = this.delegate.heightForRowAtIndex(i);
						
			CustomTableRow v = this.delegate.rowForObjectAtIndex(i);
			v.setDelegate(this);
			v.setBounds(0,this.offset+ contentView.getBounds().height, w, h);
			contentView.add(v);
			
			panels.add(v);
			Rectangle oldBounds = contentView.getBounds();
			oldBounds.height += h+offset;
			contentView.setBounds(oldBounds);
			contentView.setPreferredSize(new Dimension(contentView.getWidth(),contentView.getHeight()));
			
			
		}
		
	}
		
	public void clear(){
		
		this.selectedIndex = 0;
		contentView.removeAll();
		Rectangle r = contentView.getBounds();
		r.width = 0;
		r.height = 0;
		panels.clear();
		contentView.setLayout(null);
		contentView.setBounds(r);
		contentView.setPreferredSize(new Dimension(r.width, r.height));
	
	}

	public void setDelegate(CustomTableViewDelegate d){
		
		this.delegate = d;
		
	}
	
	public CustomTableViewDelegate getDelegate(){
		
		return this.delegate;
		
	}



	public int getSelectedIndex() {
		return selectedIndex;
	}



	public void setSelectedIndex(int selectedIndex){
		
		// < 1 because first panel is "add event"-panel
		if(selectedIndex >= this.delegate.numberOfRowsInTable()
		   || selectedIndex < 0){
			
			System.out.println("selection failure... index too high/low");
			
		}else{
						
			CustomTableRow old = panels.get(this.selectedIndex);
			old.setSelected(false);
			
			this.selectedIndex = selectedIndex;
			this.delegate.didSelectRowAtIndex(selectedIndex);
			CustomTableRow v = panels.get(selectedIndex);
			v.setSelected(true);
		
		
		}
	}
	
	@Override
	public void notifyObservers(Object obj) {
		
		for(Observer o : this.observers){
			
			o.update(this, obj);
			
		}
		
	}


	@Override
	public void removeObserver(Observer o) {

		this.observers.remove(o);
		
	}

	@Override
	public void addObserver(Observer o) {	
		if(!this.observers.contains(o))
			this.observers.add(o);
		
	}

	@Override
	public void didSelectRow(CustomTableRow r) {
		
		int index = panels.indexOf(r);
		setSelectedIndex(index);
		
	}
	
	public JPanel getContentView() {
		return contentView;
	}

	public void setContentView(JPanel contentView) {
		this.contentView = contentView;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
		this.displayRows();
	}

	@Override
	public void didEditRow(CustomTableRow r) {
		
		int index = panels.indexOf(r);
		this.delegate.didEditRowAtIndex(index);
		
	}

	@Override
	public void didDeleteRow(CustomTableRow r) {
		
		int index = panels.indexOf(r);
		this.delegate.didDeleteRowAtIndex(index);
		
	}

}
