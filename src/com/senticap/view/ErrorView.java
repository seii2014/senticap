package com.senticap.view;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

public class ErrorView extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel messageLabel;
	
	public ErrorView(String message){
		
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		messageLabel.setText(message);
		messageLabel.setFont(messageLabel.getFont().deriveFont(30.0f));
		messageLabel.setForeground(new Color(255,255,255,255));
		messageLabel.setBackground(new Color(240,20,20,255));
		
		layout.putConstraint(SpringLayout.NORTH, messageLabel, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, messageLabel, 0, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, messageLabel, 0, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.WEST, messageLabel, 0, SpringLayout.WEST, this);
		
		add(messageLabel);
	}
	
	
}
