package com.senticap.view;

public interface CustomTableViewDelegate {

	public CustomTableRow rowForObjectAtIndex(int index);
	public int numberOfRowsInTable();
	public int widthForRowAtIndex(int index);
	public int heightForRowAtIndex(int index);
	public void didSelectRowAtIndex(int panelIndex);
	public void didDeleteRowAtIndex(int index);
	public void didEditRowAtIndex(int index);
	
}
