package com.senticap.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javax.swing.JPanel;

import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGElement;
import com.kitfox.svg.SVGElementException;
import com.kitfox.svg.SVGException;
import com.kitfox.svg.SVGUniverse;
import com.kitfox.svg.animation.AnimationElement;

public class WorldMapView extends JPanel implements MouseWheelListener{
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private float mapScale = 1.0f; 

	public WorldMapView() {
    	
    	setBackground(new Color(255,255,255,255));
    	this.addMouseWheelListener(this);
    	setCursor(new Cursor(Cursor.HAND_CURSOR));

    }

   
    public void paintComponent(Graphics g) {
        super.paintComponent(g);       
        
        Graphics2D g2 = (Graphics2D) g;
        
        //turn anti-aliasing on to remove jagged edges
        g2.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        //load the world map SVG file
        File f = new File("resources/worldmap.svg");
        
        try{
        	
        	URL url = f.toURI().toURL();
        	SVGUniverse svgUniverse = new SVGUniverse();
            SVGDiagram diagram = svgUniverse.getDiagram(svgUniverse.loadSVG(url));
           
                     
            float d = (float)(this.getBounds().getWidth());
            float scale = (d/580.0f)*this.mapScale;
            g2.scale(scale, scale);
            diagram.render(g2);
            
        }catch(MalformedURLException e){
        	
        	System.out.println(e.getMessage());
        	
        }catch(SVGException e){
        	
        	System.out.println(e.getMessage());
        	
        }

    }  
    
    
    
    
    
    public void setVisible(boolean aFlag){
    	
    	super.setVisible(aFlag);
    	repaint();
    	
    }
    
    @SuppressWarnings("unused")
	private void changeAlphaForElement(SVGDiagram diagram ,String ISOCountryCode, float alpha){
    	
    	SVGElement element = diagram.getElement(ISOCountryCode);
        if(element != null){
	        try
	        {
	            if (!element.hasAttribute("opacity", AnimationElement.AT_CSS))
	            {
	            	element.addAttribute("opacity", AnimationElement.AT_CSS, ""+alpha);
	            }
	            else
	            {
	            	element.setAttribute("opacity", AnimationElement.AT_CSS, ""+alpha);
	            }
	        }
	        catch (SVGElementException e)
	        {
	            e.printStackTrace();
	        }
        }

        
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        
    
        int notches = e.getWheelRotation();
        if (notches < 0) {
            this.mapScale-=notches;
        } else {
        	this.mapScale-=notches;
        
        }
        if (e.getScrollType() == MouseWheelEvent.WHEEL_UNIT_SCROLL) {
          
        } else { //scroll type == MouseWheelEvent.WHEEL_BLOCK_SCROLL
          
        }
        
        if(this.mapScale < 1.0f)
        	this.mapScale = 1.0f;
        
        this.repaint();
     }

    
}
