package com.senticap.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

public class PreferenceTableRow extends CustomTableRow {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PreferenceTableRow(Color primaryColor, Color selectedColor) {
		super(primaryColor, selectedColor);
		// TODO Auto-generated constructor stub
	}

	protected void paintComponent(Graphics g) {
        
        int w = getWidth(); 
        int h = getHeight();
        Graphics2D g2d = (Graphics2D) g;
        
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
        		RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
        		RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        
        
        
        if(this.selected || this.hovering)
        	g2d.setColor(this.selectedColor);
        else 
        	g2d.setColor(this.primaryColor);
        
        g2d.fillRoundRect(0, 0, w, h, 0, 0);
        
       /* if(this.selected || this.hovering)
        	g2d.setColor(new Color(80,80,80,255));
        else 
        	g2d.setColor(new Color(80,80,80,255));
        
        g2d.drawRoundRect(2, 2, w-4, h-4, 20, 20);*/
        
    }
	
}
