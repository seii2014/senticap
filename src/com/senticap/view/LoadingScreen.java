package com.senticap.view;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SpringLayout;

import com.senticap.model.Globals;

public class LoadingScreen extends BlurView {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static LoadingScreen sharedInstance; 
	private String loadingText;
	private JLabel loadingLabel;
	private static int retainCount; 
	
	public static LoadingScreen getSharedInstance(){
		
		if(sharedInstance == null){
			sharedInstance = new LoadingScreen();
		}
		
		return sharedInstance;
		
	}
	
	
	private LoadingScreen(){
		
		super();
		
		this.loadingText = "Loading...";
		this.dismissable = false;
		
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		loadingLabel = new JLabel(Globals.getLocalizedStrings().getString("loading"));
		loadingLabel.setForeground(new Color(255,255,255,255));
		loadingLabel.setFont(loadingLabel.getFont().deriveFont(40.0f));
		
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, loadingLabel,
				0, 
				SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, loadingLabel,
				0, 
				SpringLayout.HORIZONTAL_CENTER, this);
		
		add(loadingLabel);
		
	}
	
	public void fadeOut(){
		retainCount--;
		if(retainCount == 0)
			super.fadeOut();
		
		
	}
	
	public void fadeIn(){
	
		if(retainCount >= 0)
			super.fadeIn();
		retainCount++;
	}

	public String getLoadingText() {
		return loadingText;
	}


	public void setLoadingText(String loadingText) {		
		this.loadingText = loadingText;
		loadingLabel.setText(loadingText);
	}
	
}
