package com.senticap.view;

import com.senticap.model.Event;

public interface EventCreationDelegate {
	
	public void eventCreationSuccessful(Event event);
	
}
