package com.senticap.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

public class EventFormButton extends JButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color backgroundColor;
	private Color selectedColor;
	private Color currentColor;
	private EventFormButton component = this;
	private String text;
	
	public EventFormButton(String title){
		
		setup(title,new Color(255,255,255,255), new Color(245,245,245,255));
		
	}
	
	public EventFormButton(String title,Color backgroundColor, Color selectedColor){
		
		setup(title,backgroundColor, selectedColor);
		
	}
	
	private void setup(String title,Color backgroundColor, Color selectedColor){
		
		
		
		this.backgroundColor = backgroundColor;
		currentColor = backgroundColor;
		this.selectedColor = selectedColor;
		this.text = title;
		
		
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {}

			@Override
			public void mouseReleased(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) {
				
				currentColor = component.selectedColor;
				component.setCursor(new Cursor(Cursor.HAND_CURSOR));
				repaint();				
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				
				currentColor = component.backgroundColor;
				repaint();
		
			}
	
		});
		
	}
	
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		this.repaint();
	}
	
	protected void paintComponent(Graphics g){
		
		Graphics2D g2d = (Graphics2D) g.create();

		int w = this.getWidth();
		int h = this.getHeight();
		
		g2d.setColor(currentColor);
		
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.fillRect(0, 0, w, h);    
	    
	    g2d.setColor(new Color(55,55,55,255));
	    
	    FontMetrics fm = g2d.getFontMetrics();
        int totalWidth = (fm.stringWidth(this.getText())) + 4;
        
        // Baseline
        int x = (getWidth() - totalWidth) / 2;
        int y = (getHeight() - fm.getHeight()) / 2;
        
        g2d.drawString(this.text, x, y + ((fm.getDescent() + fm.getAscent()) / 2)+4);
	    
		g2d.dispose();
		
	} 
	
}
