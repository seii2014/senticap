package com.senticap.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import com.senticap.model.Event;

public class CustomTableRow extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected CustomTableView tableView;
	protected boolean selected;
	protected boolean hovering;
	protected Object data;
	private CustomTableRow component = this;
	protected Color primaryColor;
	protected Color selectedColor;
	protected boolean selectable;
	
	public CustomTableRow(Color primaryColor, Color selectedColor){
		
		super();
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		this.selectable = true;
		this.primaryColor = primaryColor;
		this.selectedColor = selectedColor;
		
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		setBackground(this.primaryColor);
		
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				
				component.setSelected(true);
				component.setBackground(component.selectedColor);
				component.getDelegate().didSelectRow(component);
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) {
				
				if(!component.hovering)
					component.repaint();
				
				component.hovering = true;
						
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				component.hovering = false;
				if(!selected)
					component.repaint();
		
			}
	
		});
		
	}
	
	protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        int w = getWidth(); 
        int h = getHeight();
        Graphics2D g2d = (Graphics2D) g;
        
        if(this.selected || this.hovering)
        	g2d.setColor(this.selectedColor);
        else 
        	g2d.setColor(this.primaryColor);
        
        g2d.fillRect(0, 0, w, h);
    }
	
	public CustomTableView getDelegate() {
		return tableView;
	}

	public void setDelegate(CustomTableView delegate) {
		this.tableView = delegate;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		if(selectable)this.selected = selected;
	}

	public Object getData() {
		// TODO Auto-generated method stub
		return this.data;
	}

	public void setData(Object data) {
		
		this.data = (Event) data;
		
	}

	public boolean isSelectable() {
		return selectable;
	}

	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}
	
}
