package com.senticap.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.FocusManager;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

public class CustomTextField extends JTextField {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String placeholder;
	private Font font;
	
	public CustomTextField(String placeholder, Color borderColor){
		
		super();
		this.placeholder = placeholder;
		
		setBorder(BorderFactory.createLineBorder(new Color(250,250,250,235)));

        try
        {
      	  font = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Regular.ttf")).deriveFont(22f);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.err.println("font not loaded.  Using serif font.");
            font = new Font("serif", Font.PLAIN, 15);
        }
        
        setFont(font);
        //this border is just for the inset of the textfield text
        
        Border line = BorderFactory.createLineBorder(borderColor);
        Border empty = new EmptyBorder(0, 10, 0, 0);
        CompoundBorder border = new CompoundBorder(line, empty);
        this.setBorder(border);
        
	}
	
	protected void paintComponent(java.awt.Graphics g) {
	    
		super.paintComponent(g);
	    
		Graphics2D g2d = (Graphics2D)g.create();
		g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);
		
	    if(getText().isEmpty() && ! (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == this)){
	     
	        g2d.setFont(this.font);
	        g2d.setColor(new Color(150,150,150,255));
	        g2d.drawString(this.placeholder, 10, 32); //figure out x, y from font's FontMetrics and size of component.
	       
	    }
	    
	    g2d.dispose();
	  }
	
	
	
}
