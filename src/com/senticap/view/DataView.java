package com.senticap.view;

import java.awt.Color;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import com.senticap.model.Event;
import com.senticap.model.Globals;
import com.senticap.model.MetaData;
import com.senticap.model.Observable;
import com.senticap.model.Observer;
import com.senticap.model.TweetStatus;

public class DataView extends JPanel implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel numberOfTweetsLabel;
	private JLabel positiveSentimentLabel;
	private JLabel neutralnegativeSentimentLabel;
	private JLabel evaluationPeriodLabel;
	private Event event;
	
	public DataView() {
		
		SpringLayout layout = new SpringLayout();
		setBackground(new Color(245,245,245,255));
		setLayout(layout);
		
		
		
		numberOfTweetsLabel = new JLabel(Globals.getLocalizedStrings().getString("numberoftweets")+" : -");
		layout.putConstraint(SpringLayout.NORTH, numberOfTweetsLabel, 
				10, 
				SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, numberOfTweetsLabel, 
				10, 
				SpringLayout.WEST, this);
		
		add(numberOfTweetsLabel);
		
		positiveSentimentLabel = new JLabel(Globals.getLocalizedStrings().getString("positivesentiment")+": -");
		layout.putConstraint(SpringLayout.NORTH, positiveSentimentLabel, 
				10, 
				SpringLayout.SOUTH, numberOfTweetsLabel);
		layout.putConstraint(SpringLayout.WEST, positiveSentimentLabel, 
				0, 
				SpringLayout.WEST, numberOfTweetsLabel);
		
		add(positiveSentimentLabel);
		
		neutralnegativeSentimentLabel = new JLabel(Globals.getLocalizedStrings().getString("neutralnegativesentiment")+": -");
		layout.putConstraint(SpringLayout.NORTH, neutralnegativeSentimentLabel, 
				10, 
				SpringLayout.SOUTH, positiveSentimentLabel);
		layout.putConstraint(SpringLayout.WEST, neutralnegativeSentimentLabel, 
				0, 
				SpringLayout.WEST, positiveSentimentLabel);
		
		add(neutralnegativeSentimentLabel);
		
		evaluationPeriodLabel = new JLabel(Globals.getLocalizedStrings().getString("evaluationperiod")+": -");
		layout.putConstraint(SpringLayout.NORTH, evaluationPeriodLabel, 
				10, 
				SpringLayout.SOUTH, neutralnegativeSentimentLabel);
		layout.putConstraint(SpringLayout.WEST, evaluationPeriodLabel, 
				0, 
				SpringLayout.WEST, neutralnegativeSentimentLabel);
		
		add(evaluationPeriodLabel);
		
		
	}
	
	

	public void setEvent(Event event) {
		
		this.event = event;
		
		float percentages[] = null;
		ArrayList<TweetStatus> tweets = event.getTweets();
		if(tweets != null && tweets.size() > 0){
			percentages = MetaData.tweetMoodPercentages(tweets);
		
			if(percentages != null){
				this.numberOfTweetsLabel.setText(Globals.getLocalizedStrings().getString("numberoftweets")+": "+tweets.size());
				this.positiveSentimentLabel.setText(Globals.getLocalizedStrings().getString("positivesentiment")+": "+percentages[0]+"%");
				this.neutralnegativeSentimentLabel.setText(Globals.getLocalizedStrings().getString("neutralnegativesentiment")+": "+percentages[1]+"%");
				
			}
			
			Date endDate = tweets.get(0).getCreatedAt();
			Date startDate = tweets.get(tweets.size()-1).getCreatedAt();
			
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");     
			String evalStart = df.format(startDate);
			String evalEnd = df.format(endDate);
			
			this.evaluationPeriodLabel.setText(Globals.getLocalizedStrings().getString("evaluationperiod")+": "+evalStart+" - "+evalEnd);
		}
		
		
	}


	@Override
	public void update(Observable o, Object obj) {
		
		Event event = (Event) obj;
		// event has changed, load tweets for this 
		// particular event
		if(event != null)
		{
			this.setEvent(event);
			this.event.addObserver(this);
		}
		
	}



	public Event getEvent() {
		return event;
	}

}
