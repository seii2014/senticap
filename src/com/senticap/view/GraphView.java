package com.senticap.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import java.awt.geom.RoundRectangle2D;
import java.util.Date;

import javax.swing.JPanel;

import com.senticap.model.Event;
import com.senticap.model.GraphData;
import com.senticap.model.MetaData;
import com.senticap.model.Observable;
import com.senticap.model.Observer;

public class GraphView extends JPanel implements Observer{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Event event;
	private GraphData data;
	// x- and y-Unit are the distances for 1 unit on 
	// the x and y axes on the graph respectively
	
	private Color graphColor = new Color(10,200,80,230);
	private Color bgColor = new Color(225,225,225,255);
	
	private float xUnit;
	private float yUnit;
	
	public GraphView(GraphData data){
		
		this.data = data;
		
	}
	
	public GraphData getData() {
		return data;
	}

	public void setData(GraphData data) {
		this.data = data;
		repaint();
	}
	
	protected void paintComponent(Graphics g){
		
		Graphics2D g2d = (Graphics2D)g.create();
		
        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);
        
        int cornerRadius = 0;
        
        g2d.setColor(this.bgColor);
        g2d.setClip(new RoundRectangle2D.Float(0, 0, getWidth(), getHeight(), cornerRadius, cornerRadius));
        g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
        g2d.setColor(this.graphColor);
        
		GeneralPath path = new GeneralPath();
		path.moveTo(0, this.getHeight());
		if(this.data != null && this.data.getGroupedTweets() != null){
			int counter = 0;
			int count = this.data.getGroupedTweets().keySet().size();
		
			for(Date date : this.data.getGroupedTweets().keySet()){
				counter++;
				float percentages[] = MetaData.tweetMoodPercentages(this.data.getGroupedTweets().get(date));
				path.lineTo(((float)this.getWidth()/(float)count)*(float)counter, this.getHeight()*((percentages[MetaData.MOOD.kMoodNeutralNegative])/100.0f));
			}
			
		}
		path.lineTo(this.getWidth(), this.getHeight());
		path.closePath();
		g2d.fill(path);
		
		g2d.dispose();
		
	}

	public Color getGraphColor() {
		return graphColor;
	}

	public void setGraphColor(Color graphColor) {
		this.graphColor = graphColor;
		repaint();
	}

	public Color getBgColor() {
		return bgColor;
		
	}

	public void setBgColor(Color bgColor) {
		this.bgColor = bgColor;
		repaint();
	}

	public float getyUnit() {
		return yUnit;
	}

	public void setyUnit(float yUnit) {
		this.yUnit = yUnit;
	}

	public float getxUnit() {
		return xUnit;
	}

	public void setxUnit(float xUnit) {
		this.xUnit = xUnit;
	}

	@Override
	public void update(Observable o, Object obj) {
		
		Event event = (Event) obj;
		// event has changed, load tweets for this 
		// particular event
		if(event != null)
		{
			if(event.getTweets() != null) 
				this.data.setTweets(event.getTweets());
			this.event = event;
			this.event.addObserver(this);
		}

	}

}
