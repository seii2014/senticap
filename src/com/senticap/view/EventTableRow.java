package com.senticap.view;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import com.senticap.model.Event;
import com.senticap.model.GraphData;
import com.senticap.model.MetaData;
import com.senticap.model.Observable;
import com.senticap.model.Observer;

public class EventTableRow extends CustomTableRow implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel nameLabel;
	private JLabel tagsLabel;
	private JLabel positivePercentageLabel;
	private JLabel neutralnegativePercentageLabel;
	private boolean isExpanded;
	EventTableRow component = this;
	private Event event;
	private GraphView graphView;
	
	public EventTableRow(Event e,Color primaryColor, Color selectedColor){
		
		super(primaryColor,selectedColor);
		
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		this.event = e;
		this.event.addObserver(this);
		this.setBackground(new Color(27,36,50,255));
		
		

        //event name 
		nameLabel = new JLabel(this.event.getName());
		nameLabel.setForeground(new Color(255,255,255,200));
		nameLabel.setBounds(10,18,301,24);
		nameLabel.setFont(new Font("Oxygen-Regular", Font.PLAIN, 22));
		
		//hashtags label
		tagsLabel = new JLabel(this.event.getHashtagsString(" "));
		tagsLabel.setForeground(new Color(168,191,218,255));
		tagsLabel.setBounds(10,18,301,24);
	
		// add + in front if positive
		positivePercentageLabel = new JLabel();
		positivePercentageLabel.setFont(new Font("Oxygen-Regular", Font.PLAIN, 12));
		positivePercentageLabel.setForeground(new Color(10,230,20,255));
			
			
		neutralnegativePercentageLabel = new JLabel();
		neutralnegativePercentageLabel.setFont(new Font("Oxygen-Regular", Font.PLAIN, 12));
		neutralnegativePercentageLabel.setForeground(new Color(255,150,0,255));
		
		JLabel separatorLabel = new JLabel("/");
		
		this.updatePercentages();
		
		CustomButton editButton = new CustomButton("edit_icon.png",null);
		editButton.setBackground(new Color(0,0,0,0));
		editButton.setPreferredSize(new Dimension(20,20));
		
		editButton.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				
				component.tableView.didEditRow(component);
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			
			
			
		});
		
		CustomButton deleteButton = new CustomButton("delete_icon.png",null);
		deleteButton.setBackground(new Color(0,0,0,0));
		deleteButton.setPreferredSize(new Dimension(20,20));
		
		deleteButton.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				
				component.tableView.didDeleteRow(component);
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			
			
			
		});
		
		this.graphView = new GraphView(new GraphData(this.event.getTweets()));
		this.graphView.setBgColor(new Color(0,0,0,10));
		this.graphView.setGraphColor(new Color(68,196,250,185));
		graphView.setPreferredSize(new Dimension(50,30));
		
		layout.putConstraint(SpringLayout.NORTH, graphView,
                10,
                SpringLayout.NORTH, this);
		
		layout.putConstraint(SpringLayout.EAST, graphView,
                -10,
                SpringLayout.EAST, this);		
		
		layout.putConstraint(SpringLayout.WEST, editButton,
                10,
                SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.SOUTH, editButton,
                -10,
                SpringLayout.SOUTH, this);
		
		layout.putConstraint(SpringLayout.WEST, deleteButton,
                0,
                SpringLayout.EAST, editButton);
		layout.putConstraint(SpringLayout.SOUTH, deleteButton,
                0,
                SpringLayout.SOUTH, editButton);
		
		
		layout.putConstraint(SpringLayout.NORTH, nameLabel,
                12,
                SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.NORTH, tagsLabel,
                0,
                SpringLayout.SOUTH, nameLabel);
		
		layout.putConstraint(SpringLayout.WEST, nameLabel,
                10,
                SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.WEST, tagsLabel,
                0,
                SpringLayout.WEST, nameLabel);
		
		layout.putConstraint(SpringLayout.EAST, neutralnegativePercentageLabel,
                -10,
                SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, neutralnegativePercentageLabel,
                -8,
                SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.EAST, separatorLabel,
                -3,
                SpringLayout.WEST, neutralnegativePercentageLabel);
		layout.putConstraint(SpringLayout.EAST, positivePercentageLabel,
                -3,
                SpringLayout.WEST, separatorLabel);
		layout.putConstraint(SpringLayout.NORTH, positivePercentageLabel,
                0,
                SpringLayout.NORTH, neutralnegativePercentageLabel);
		layout.putConstraint(SpringLayout.NORTH, separatorLabel,
                -3,
                SpringLayout.NORTH, neutralnegativePercentageLabel);
		
		this.add(graphView);
		this.add(editButton);
		this.add(deleteButton);
		this.add(positivePercentageLabel);
		this.add(neutralnegativePercentageLabel);
		this.add(tagsLabel);
		this.add(nameLabel);
		this.add(separatorLabel);
	}
	
	private void updatePercentages(){
		
		float[] percentages  = {0.0f, 0.0f};
		
		if(this.event.getTweets() != null)
			percentages = MetaData.tweetMoodPercentages(this.event.getTweets());
		
		// add + in front if positive
		positivePercentageLabel.setText(String.format("%d%%", 
									 Math.round(percentages[MetaData.MOOD.kMoodPositive])));

		neutralnegativePercentageLabel.setText(String.format("%d%%", 
				 Math.round(percentages[MetaData.MOOD.kMoodNeutralNegative])));

		
	}
	
	public JLabel getNameLabel(){
		
		return this.nameLabel;
		
	}
	
	public void setNameLabel(JLabel nameLabel){
		
		this.nameLabel = nameLabel;
		
	}
	
	
	public Dimension getPreferredSize(){
        //Hard coded preferred size - but you'd probably want 
        //to calculate it based on the panel's content 
        return new Dimension(this.getWidth(),this.getHeight());
    }

	public boolean isExpanded() {
		return isExpanded;
	}



	public void setExpanded(boolean isExpanded) {
		this.isExpanded = isExpanded;
	}

	

	public void setSelected(boolean selected) {
		
		super.setSelected(selected);
		
		if(selected){
	
			this.setBackground(new Color(32,54,70,255));
			
		}else{
			
			this.setBackground(new Color(27,36,50,255));
			
		}
	}

	@Override
	public void update(Observable o, Object obj) {
		
		this.updatePercentages();
		if(obj != null && ((Event)obj).getTweets() != null)
			this.graphView.setData(new GraphData(((Event)obj).getTweets()));
		
	}

	
}
