package com.senticap.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Rectangle;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import com.senticap.model.Globals;

public class CustomButton extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected Color color;
	protected Color hoverColor;
	protected CustomButton component = this;
	protected boolean selected;
	
	public CustomButton(String iconRef, String title){
		
		super();
		
		this.color = new Color(235,235,235,255);
		this.hoverColor = new Color(245,245,245,255);
				
		setBackground(this.color);
		
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		
		SpringLayout layout = new SpringLayout();
		JLabel buttonTextLabel = new JLabel();
		
		setLayout(layout);
		
		ImageIcon image = new ImageIcon(Globals.prependBasePath(Globals.FOLDER.kImageFolder, iconRef));
		JLabel imageLabel = new JLabel(image);
		imageLabel.setBounds(new Rectangle(0,0,this.getBounds().width, 100));
		imageLabel.setHorizontalAlignment(JLabel.CENTER);
		imageLabel.setVerticalAlignment(JLabel.CENTER);
		
		
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, imageLabel,
				0, 
				SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, imageLabel,
				0, 
				SpringLayout.HORIZONTAL_CENTER, this);
		
		if(title != null){
			
			buttonTextLabel.setBounds(new Rectangle(0,0,this.getBounds().width, 20));
			buttonTextLabel.setText(title);
			buttonTextLabel.setHorizontalTextPosition(JLabel.CENTER);
			
			
			layout.putConstraint(SpringLayout.NORTH, buttonTextLabel,
					0, 
					SpringLayout.SOUTH, imageLabel);
			layout.putConstraint(SpringLayout.WEST, imageLabel,
					0, 
					SpringLayout.WEST, this);
			layout.putConstraint(SpringLayout.EAST, this,
					0, 
					SpringLayout.EAST, imageLabel);
			
			add(buttonTextLabel);
		
		}
		 
		add(imageLabel);
		
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Color getHoverColor() {
		return hoverColor;
	}

	public void setHoverColor(Color hoverColor) {
		this.hoverColor = hoverColor;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.setBackground(color);
		this.color = color;
	}

}
