package com.senticap.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.RoundRectangle2D;
import java.io.File;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import com.senticap.controller.EventCreationController;
import com.senticap.model.Event;
import com.senticap.model.Globals;

public class EventForm extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final EventForm component = this;
	private EventFormDelegate delegate;
	private CustomTextField eventNameField;
	private CustomTextField hashTagField;
	private Event event;
	
	public EventForm(Event event){
		
		super();
		
		this.event = event;
		
		Font boldFont;
	    //Font regularFont;
	    SpringLayout layout = new SpringLayout();
	    setLayout(layout);
	    setPreferredSize(new Dimension(500,500));
	    setBackground(new Color(255,255,255,255));
	    
	    try {
	    	  boldFont = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Bold.ttf")).deriveFont(30f);
	    	  //regularFont = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Regular.ttf")).deriveFont(16f);
	   	} catch (Exception e) {
	    	  boldFont = new Font("Arial", Font.PLAIN, 16);
	    	  //regularFont = new Font("Arial", Font.PLAIN, 16);
	    }
	    
	    
	    GraphicsEnvironment genv = GraphicsEnvironment.getLocalGraphicsEnvironment();
	    genv.registerFont(boldFont);
	    
	    String eventActionString = Globals.getLocalizedStrings().getString("add_event");
	    if(this.event != null){
	    	
	    	eventActionString = Globals.getLocalizedStrings().getString("edit_event");
	    	
	    }
	    
	    JLabel eventLabel = new JLabel(eventActionString);
	    eventLabel.setHorizontalAlignment(SwingConstants.CENTER);
	    eventLabel.setFont(boldFont);
	    eventLabel.setForeground(new Color(60,60,60,255));
	    eventLabel.setBackground(new Color(0,0,0,0));
	    eventLabel.setOpaque(true);
	    
	    EventFormButton cancelButton = new EventFormButton(Globals.getLocalizedStrings().getString("cancel"));
	    cancelButton.setPreferredSize(new Dimension(200,40));
	    cancelButton.addMouseListener(new MouseListener() {
 
            public void mouseClicked(MouseEvent e)
            {
            	((EventCreationController)component.getParent()).fadeOut();
           
            }

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
        }); 
 
	    String saveButtonString = "";
	    if(this.event != null)
	    	saveButtonString = Globals.getLocalizedStrings().getString("save_event");
	    else 
	    	saveButtonString = Globals.getLocalizedStrings().getString("add_event");
	    
	    EventFormButton saveButton = new EventFormButton(saveButtonString);
	    saveButton.setPreferredSize(new Dimension(200,40));
	    saveButton.addMouseListener(new MouseListener() {
 
            public void mouseClicked(MouseEvent e)
            {
            	
                Event event = generateEvent();
               	if(component.validateEvent(event))
               		component.delegate.saveButtonClicked(event);
               	else
               		System.out.println("Invalid characters!");
           
            }

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
        });      
	    
	    eventNameField = new CustomTextField(Globals.getLocalizedStrings().getString("event_name"),new Color(200,200,200,255));
	    eventNameField.setPreferredSize(new Dimension(200,50));
	    if(event != null)
	    	eventNameField.setText(event.getName());
	    
	    hashTagField = new CustomTextField("Hashtags (i.e. #HT1,#test,...)",new Color(200,200,200,255));
	    hashTagField.setPreferredSize(new Dimension(200,50));
	    if(event != null)
	    	hashTagField.setText(event.getHashtagsString(", "));
	    
	    layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, eventLabel,
                0,
                SpringLayout.HORIZONTAL_CENTER, this);
	    layout.putConstraint(SpringLayout.NORTH, eventLabel,
                20,
                SpringLayout.NORTH, this);
	    layout.putConstraint(SpringLayout.WEST, eventNameField,
                4,
                SpringLayout.WEST, this);
	    layout.putConstraint(SpringLayout.EAST, eventNameField,
                -3,
                SpringLayout.EAST, this);
	    layout.putConstraint(SpringLayout.NORTH, eventNameField,
                10,
                SpringLayout.SOUTH, eventLabel);
	    
	    layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, hashTagField,
                0,
                SpringLayout.HORIZONTAL_CENTER, this);
	    layout.putConstraint(SpringLayout.WEST, hashTagField,
                4,
                SpringLayout.WEST, this);
	    layout.putConstraint(SpringLayout.EAST, hashTagField,
                -3,
                SpringLayout.EAST, this);
	    layout.putConstraint(SpringLayout.NORTH, hashTagField,
                2,
                SpringLayout.SOUTH, eventNameField);
	    
	    layout.putConstraint(SpringLayout.SOUTH, cancelButton,
                -10,
                SpringLayout.SOUTH, this);
	    layout.putConstraint(SpringLayout.WEST, cancelButton,
                5,
                SpringLayout.WEST, this);
	    layout.putConstraint(SpringLayout.EAST, cancelButton,
                -1,
                SpringLayout.HORIZONTAL_CENTER, this);
	    
	    layout.putConstraint(SpringLayout.SOUTH, saveButton,
                -10,
                SpringLayout.SOUTH, this);
	    layout.putConstraint(SpringLayout.EAST, saveButton,
                -5,
                SpringLayout.EAST, this);
	    layout.putConstraint(SpringLayout.WEST, saveButton,
                1,
                SpringLayout.HORIZONTAL_CENTER, this);
	    
	    add(eventLabel);
	    add(eventNameField);
	    add(hashTagField);
	    add(cancelButton);
	    add(saveButton);
	}
	
	private boolean validateEvent(Event event){
		
		String eventRegex = "^[a-zA-Z0-9 ]*$";
		String hashtagRegex = "(#[a-zA-Z_0-9]+,{0,1} *)*";
		String hashtagString = event.getHashtagsString(",");
		String eventName = event.getName();
		
		System.out.println("event name "+eventName+ (eventName.matches(eventRegex) ? " does " : " does not ") + "match regex");
		System.out.println("event tags "+hashtagString+ (hashtagString.matches(hashtagRegex) ? " do " : " do not ") + "match regex");
		
		return (eventName.matches(eventRegex) && hashtagString.matches(hashtagRegex));
	}
	
	private Event generateEvent(){
	
		String hashtagString = this.hashTagField.getText().replace(" ", "");
		String eventName = eventNameField.getText();
		
		
		if(this.event != null){
			
			this.event.setName(eventName);
			this.event.setHashtags(Event.getHashtagsFromString(hashtagString, ","));
			return this.event;
			
		}
		else{
			
			return new Event(-1,eventName,Event.getHashtagsFromString(hashtagString, ","));
			
		}
		
		
		
	}
	
	protected void paintComponent(Graphics g){
		
		Graphics2D g2d = (Graphics2D) g.create();

		int rectWidth = this.getWidth();
		int rectHeight = this.getHeight();
		int inset = 4;
		
		g2d.setColor(new Color(255,255,255,220));
		RoundRectangle2D r = new RoundRectangle2D.Double(inset, 2,
                rectWidth-inset*2,
                rectHeight-inset*2,
                10, 10); 
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.fill(r);
	    
	    g2d.setColor(new Color(255,255,255,255));
	    g2d.setStroke(new BasicStroke(1));
	    g2d.draw(r);
	    
		
		g2d.dispose();
		
	} 
	
	

	public EventFormDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(EventFormDelegate delegate) {
		this.delegate = delegate;
	}
	
}
