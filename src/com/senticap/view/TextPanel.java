package com.senticap.view;

import java.awt.*;
import java.io.File;

import javax.swing.*;


public class TextPanel extends CustomTableRow{
	
	/**
	 * This is the panel that (on click) will open 
	 * the event creating view. 
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel titleLabel;
	
	public JLabel getTitleLabel() {
		return titleLabel;
	}

	public void setTitleLabel(JLabel titleLabel) {
		this.titleLabel = titleLabel;
	}

	public TextPanel(String title, Color labelColor, Color primaryColor, Color selectedColor){
		
		super(primaryColor, selectedColor);
		this.setLayout(null);
		this.setBackground(primaryColor);
		
		
		
		
		Font titleFont = null;

        try
        {
            //InputStream is = Constants.getPathForFont("Oxygen-Light.ttf");
        	titleFont = Font.createFont(Font.TRUETYPE_FONT, new File("resources/font/Oxygen-Bold.ttf")).deriveFont(20f);
            //titleFont = Font.createFont(Font.TRUETYPE_FONT, is).deriveFont(Font.PLAIN, 20);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            System.err.println("font not loaded.  Using serif font.");
            titleFont = new Font("serif", Font.PLAIN, 18);
        }
		
		titleLabel = new JLabel(title);
		titleLabel.setBounds(0,0,230,80);
		titleLabel.setFont(titleFont);
		titleLabel.setForeground(labelColor);
		titleLabel.setPreferredSize(new Dimension(this.getWidth(),titleLabel.getHeight()));
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(titleLabel);
		
	}


	
}
