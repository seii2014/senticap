package com.senticap.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class RoundImage extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage image;
	
	public RoundImage(BufferedImage image){
		
		this.image = image;
		setBackground(new Color(0,0,0,0));
		setOpaque(false);
		
	}
	
	public BufferedImage getImage() {
		return image;
	}

	public void setImage(BufferedImage image) {
		this.image = image;
		repaint();
	}

	protected void paintComponent(Graphics g) {
        
		super.paintComponent(g);
        
        Graphics2D graphics = (Graphics2D) g;

        RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        qualityHints.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
        graphics.setRenderingHints(qualityHints);
        
        
        graphics.setClip(new RoundRectangle2D.Float(0, 0, getWidth(), getHeight(), getWidth(), getHeight()));
        graphics.drawImage(image, 0, 0, this);//draw the rounded image
        
        graphics.dispose();
        
	}

}
