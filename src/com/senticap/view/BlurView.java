package com.senticap.view;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.jdesktop.swingx.image.GaussianBlurFilter;
import org.jdesktop.swingx.util.GraphicsUtilities;

public class BlurView extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage blurBuffer;
    private BufferedImage backBuffer;
    private float alpha = 0.0f;
    private BlurView component = this;
    protected boolean dismissable;
    protected JComponent connectedComponent;

    public BlurView(){
    	
    	this.dismissable = true;
    	this.setBackground(new Color(0,0,0,0));
    	this.setOpaque(false);
    	
    	addMouseListener(new MouseListener(){

 			@Override
 			public void mouseClicked(MouseEvent e) {
 				
 				if(component.dismissable)
 					component.fadeOut();
 				
 			}
 			@Override
 			public void mousePressed(MouseEvent e) {}
 			@Override
 			public void mouseReleased(MouseEvent e) {}
 			@Override
 			public void mouseEntered(MouseEvent e) {}
 			@Override
 			public void mouseExited(MouseEvent e) {}
 	
 			
 		});
    	
    }
    
	    
	    protected void paintComponent(Graphics g) {
	        if (isVisible()) {
	            Graphics2D g2 = (Graphics2D) g.create();

	            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	                    RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	            g2.drawImage(backBuffer, 0, 0, null);

	            g2.setComposite(AlphaComposite.SrcOver.derive(alpha));
	            g2.drawImage(blurBuffer, 0, 0, getWidth(), getHeight(), null);
	            g2.setColor(new Color(5,14,20,150));
	            g2.fillRect(0, 0, this.getWidth(), this.getHeight());
	            g2.dispose();
	        }
	    }
	    
	    @SuppressWarnings("unused")
		private void createBlur() {
	        JRootPane root = SwingUtilities.getRootPane(this);
	        blurBuffer = GraphicsUtilities.createCompatibleImage(
	            root.getWidth(), root.getHeight());
	        Graphics2D g2 = blurBuffer.createGraphics();
	        root.paint(g2);
	        g2.dispose();

	        backBuffer = blurBuffer;

	        blurBuffer = GraphicsUtilities.createThumbnailFast(
	            blurBuffer, getWidth() / 2);
	        blurBuffer = new GaussianBlurFilter(1).filter(blurBuffer, null);
	    }
	    
	    private float getAlpha() {
	        return alpha;
	    }

	    private void setAlpha(float alpha) {
	    	
	    	this.alpha = alpha;
	        repaint();
	    }

	    public void fadeIn() {
	        
	    	//createBlur();
	        setVisible(true);
	        
	        Timer timer = new Timer(10, new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	            	
	            	component.setAlpha(component.getAlpha()+0.05f);
	                if (alpha > 1) {
	                	((Timer)e.getSource()).stop();
	                    component.setAlpha(1.0f);
	           				
	                    
	                }
	            }


	        });
	        timer.setRepeats(true);
	        timer.start();
	        
	    }
	    
	    public void hideConnectedComponent(){
	    	
	    	if(this.connectedComponent == null)
	    		return;
	    	
			Timer timer = new Timer(10, new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	            	
	                component.connectedComponent.setLocation(
	                			new Point(component.connectedComponent.getLocation().x,
	                					component.connectedComponent.getLocation().y *= 1.1));
	            	
	            }


	        });
	        timer.setRepeats(true);
	        timer.start();
			
		}
	    
	    public void fadeOut(){
	    	
	    	this.hideConnectedComponent();
	    	Timer timer = new Timer(26, new ActionListener() {
	            @Override
	            public void actionPerformed(ActionEvent e) {
	            	
	            	component.setAlpha(component.getAlpha()-0.05f);
	                if (alpha < 0) {
	                	
	                	//stop the timer
	                	((Timer)e.getSource()).stop();
	                    component.setAlpha(0.0f);
	                    component.setVisible(false);
	                    //remove from parent               
	                    if(component != null && component.getParent() != null)
	                    	component.getParent().remove(component);
	     				
	                    
	                } else if (alpha > 1) {
	                	component.setAlpha(1.0f);  
	                }
	            }


	        });
	        timer.setRepeats(true);
	        timer.start();
	    	
	    }
	    
	    

		public boolean isDismissable() {
			return dismissable;
		}

		public void setDismissable(boolean dismissable) {
			this.dismissable = dismissable;
		}


		public JComponent getConnectedComponent() {
			return connectedComponent;
		}


		public void setConnectedComponent(JComponent connectedComponent) {
			this.connectedComponent = connectedComponent;
		}

}
