package com.senticap.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.geom.RoundRectangle2D;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.senticap.controller.MultipleViewProtocol;

public class HorizontalButtonGroup extends JPanel implements KeyListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int selectedIndex;
	private ArrayList<RoundedButton> buttons;
	private MultipleViewProtocol delegate;
	
	public HorizontalButtonGroup(){
		
		setOpaque(false);
		buttons = new ArrayList<RoundedButton>();
		setPreferredSize(new Dimension(400,42));
		setBackground(new Color(0,0,0,0));
		
		 // add key listener for left/right events 
        this.addKeyListener(this);
        this.setFocusable(true);
		
	}
	
	public void display(){
		
		this.removeAll();
		//-10 because otherwise, buttons won't fit
		int buttonWidth = (int)(this.getPreferredSize().width/this.buttons.size())-10;
		
		for(RoundedButton b : this.buttons){

			b.setPreferredSize(new Dimension(buttonWidth,this.getPreferredSize().height-6));
			add(b);
			
			
		}
		
	}
	
	
	
	public void addButton(RoundedButton b){
		
		this.buttons.add(b);
		b.setButtonGroup(this);
		
		
	}
	
	protected void paintComponent(Graphics g) {
        
		super.paintComponent(g);       
        
		Graphics2D g2d = (Graphics2D) g.create();
		int rectWidth = this.getWidth();
		int rectHeight = this.getHeight();
		int inset = 4;
		RoundRectangle2D r = new RoundRectangle2D.Double(inset, inset,
                rectWidth-inset*2,
                rectHeight-inset*2,
                20, 20); 
	    g2d.setColor(new Color(240,240,240,255));
	    g2d.fill(r);
	    g2d.setColor(new Color(230,230,230,255));
	    g2d.setStroke(new BasicStroke(1));
	    g2d.draw(r);
		
	    g2d.dispose();
        
    }

	public int getSelectedIndex() {
		return selectedIndex;
	}
	
	public void selectButton(RoundedButton button){
		
		setSelectedIndex(buttons.indexOf(button));
		
	}
	
	public void setSelectedIndex(int selectedIndex) {
		
		//notify delegate of changes
		this.delegate.selectionChanged(this,selectedIndex);
		
		//deactivate old button before setting new index
		RoundedButton oldSelectedButton = buttons.get(this.selectedIndex);
		oldSelectedButton.setActive(false);
		this.selectedIndex = selectedIndex;
		
		buttons.get(this.selectedIndex).setActive(true);
		
	}

	public ArrayList<RoundedButton> getButtons() {
		return buttons;
	}

	public void setButtons(ArrayList<RoundedButton> buttons) {
		this.buttons = buttons;
	}

	public MultipleViewProtocol getDelegate() {
		return delegate;
	}

	public void setDelegate(MultipleViewProtocol delegate) {
		this.delegate = delegate;
	}

	public void keyTyped(KeyEvent e) {
        //displayInfo(e, "KEY TYPED: ");
    }

    /** Handle the key-pressed event from the text field. */
    public void keyPressed(KeyEvent e) {
        displayInfo(e, "KEY PRESSED: ");
    }

    /** Handle the key-released event from the text field. */
    public void keyReleased(KeyEvent e) {
        //displayInfo(e, "KEY RELEASED: ");
    }
    
    private void displayInfo(KeyEvent e, String keyStatus){
        
        //You should only rely on the key char if the event
        //is a key typed event.
        int id = e.getID();
        int keyCode = -1;
        
        if (id == KeyEvent.KEY_TYPED) {
            //char c = e.getKeyChar();
            //keyString = "key character = '" + c + "'";
        } else {
            keyCode = e.getKeyCode();
        }
        
        //up arrow pressed
       	switch(keyCode){
       		
       		case 39:
       			setSelectedIndex(selectedIndex-1);
       			break;
       		case 41:
       			setSelectedIndex(selectedIndex+1);
       			break;
       		default:
       			break;
       	
       	}
        	
        
        //Display information about the KeyEvent...
    }


}
