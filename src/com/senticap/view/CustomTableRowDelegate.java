package com.senticap.view;

public interface CustomTableRowDelegate {
	
	public void didSelectRow(CustomTableRow r);
	public void didEditRow(CustomTableRow r);
	public void didDeleteRow(CustomTableRow r);
	
}
