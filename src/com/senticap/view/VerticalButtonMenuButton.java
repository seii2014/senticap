package com.senticap.view;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class VerticalButtonMenuButton extends CustomButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private VerticalButtonMenuButton component = this;
	private VerticalButtonMenu menu;
	
	public VerticalButtonMenuButton(String iconRef, String title){
		
		super(iconRef, title);
			
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
								
				component.setSelected(true);
				component.getMenu().selectButton(component);
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) {
				
				component.setBackground(component.hoverColor);
		
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				
				if(!component.isSelected())
					component.setBackground(component.color);
		
			}
	
		});
		
		
		
	}


	public VerticalButtonMenu getMenu() {
		return menu;
	}

	public void setMenu(VerticalButtonMenu menu) {
		this.menu = menu;
	}


	public void setSelected(boolean selected) {
		
		this.selected = selected;
		
		if(selected)
			this.setBackground(this.hoverColor);
		else 
			this.setBackground(this.color);
		
	}
	
}
