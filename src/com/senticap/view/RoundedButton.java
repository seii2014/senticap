package com.senticap.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class RoundedButton extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean active;
	private RoundedButton thisButton = this;
	private HorizontalButtonGroup buttonGroup;
	private Color color;
	private Color activeColor;
	private Color inactiveColor;
	
	public RoundedButton(String title){
		
		super();
		setOpaque(false);
		JLabel titleLabel = new JLabel(title);
		add(titleLabel);
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		color = new Color(0,0,0,0);
		activeColor = new Color(255,255,255,255);
		inactiveColor = new Color(0,0,0,0);
		
		this.addMouseListener(new MouseListener(){

			@Override
			public void mouseClicked(MouseEvent e) {
				
				
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				thisButton.getButtonGroup().selectButton(thisButton);
		
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				
				if(!active){
					color = new Color(255,255,255,140);
					repaint();
				}
			}

			@Override
			public void mouseExited(MouseEvent e) {
				
				if(!active){
					color = new Color(0,0,0,0);
					repaint();
				}
			}
	
			
		});
		
	}
	
	protected void paintComponent(Graphics g){
		
	
			
			Graphics2D g2d = (Graphics2D)g.create();
			
			int rectWidth = this.getWidth();
			int rectHeight = this.getHeight();
			int inset = 4;
			g2d.setColor(color);
			RoundRectangle2D r = new RoundRectangle2D.Double(inset, 2,
	                rectWidth-inset*2,
	                rectHeight-inset*2,
	                20, 20); 
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_ON);
		    g2d.fill(r);
		    g2d.setColor(new Color(230,230,230,255));
		    g2d.setStroke(new BasicStroke(1));
		    g2d.draw(r);
			
			g2d.dispose();
			
		

		
	}
	

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		
		this.active = active;
		if(!active)color = inactiveColor;
		else color = activeColor;
		repaint();
		
	}

	public HorizontalButtonGroup getButtonGroup() {
		return buttonGroup;
	}

	public void setButtonGroup(HorizontalButtonGroup buttonGroup) {
		this.buttonGroup = buttonGroup;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getInactiveColor() {
		return inactiveColor;
	}

	public void setInactiveColor(Color inactiveColor) {
		this.inactiveColor = inactiveColor;
	}
	
	public Color getActiveColor() {
		return activeColor;
	}

	public void setActiveColor(Color activeColor) {
		this.activeColor = activeColor;
	}
	
}
