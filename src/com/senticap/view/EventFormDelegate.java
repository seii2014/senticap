package com.senticap.view;

public interface EventFormDelegate {
	
	public void saveButtonClicked(Object object);
	public void cancelButtonClicked();

}
