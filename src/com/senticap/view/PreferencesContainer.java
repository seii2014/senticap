package com.senticap.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JPanel;
import javax.swing.Timer;

public class PreferencesContainer extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PreferencesContainer component = this;
	
	public PreferencesContainer(){
		
		
		
	}
	

	protected void paintComponent(Graphics g){
		
		Graphics2D g2d = (Graphics2D) g.create();

		int rectWidth = this.getWidth();
		int rectHeight = this.getHeight();
		int inset = 4;
		
		g2d.setColor(new Color(255,255,255,220));
		RoundRectangle2D r = new RoundRectangle2D.Double(inset, 2,
                rectWidth-inset*2,
                rectHeight-inset*2,
                10, 10); 
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
	    g2d.fill(r);
	    
	    g2d.setColor(new Color(255,255,255,255));
	    g2d.setStroke(new BasicStroke(1));
	    g2d.draw(r);
	    
		
		g2d.dispose();
		
	} 
	
	public void hide(){

		Timer timer = new Timer(10, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
         
                component.setLocation(
                			new Point(component.getLocation().x,
                					  component.getLocation().y *= 1.1));
            	
            }


        });
        timer.setRepeats(true);
        timer.start();
		
	}

	
}
