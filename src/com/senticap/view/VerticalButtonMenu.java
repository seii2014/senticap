package com.senticap.view;

import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.senticap.controller.MultipleViewProtocol;

public class VerticalButtonMenu extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<VerticalButtonMenuButton> buttons;
	private int selectedIndex;
	private MultipleViewProtocol delegate;
	
	public VerticalButtonMenu(){
		
		buttons = new ArrayList<VerticalButtonMenuButton>();
		setLayout(null);
		setBackground(new Color(205,205,205,255));
	
	}
	
	public void addButton(String iconRef, String title){
		
		Rectangle currentBounds = this.getBounds();
		VerticalButtonMenuButton button = new VerticalButtonMenuButton(iconRef,title);
		button.setBounds(new Rectangle(0,currentBounds.height,90,90));
		buttons.add(button);
		setBounds(new Rectangle(0,0,90,currentBounds.height+90));
		button.setMenu(this);
		add(button);
		
	}
	
	public ArrayList<VerticalButtonMenuButton> getButtons() {
		return buttons;
	}
	public void setButtons(ArrayList<VerticalButtonMenuButton> buttons) {
		this.buttons = buttons;
	}

	public int getSelectedIndex() {
		return selectedIndex;
	}

	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	
	public void selectButton(VerticalButtonMenuButton button){
		
		VerticalButtonMenuButton prevSelectedButton = this.buttons.get(this.selectedIndex);
		if(prevSelectedButton != button){
			prevSelectedButton.setSelected(false);
			int index = this.buttons.indexOf(button);
			setSelectedIndex(index);
			this.delegate.selectionChanged(this, index);
		}
		
	}

	public MultipleViewProtocol getDelegate() {
		return delegate;
	}

	public void setDelegate(MultipleViewProtocol delegate) {
		this.delegate = delegate;
	}
	
}
