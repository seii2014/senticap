package com.senticap.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints; 
import javax.swing.JPanel;

public class CircleView extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color color;
	
	public CircleView(){
		
		this.setBackground(new Color(0,0,0,0));
		this.setOpaque(false);
	}
	
	protected void paintComponent(Graphics g){
		
		super.paintComponent(g);
	    
	    Graphics2D graphics = (Graphics2D) g;
	
	    RenderingHints qualityHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
	    qualityHints.put(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_QUALITY);
	    graphics.setRenderingHints(qualityHints);
	   
	    graphics.setColor(this.color);
	    graphics.fillOval(1, 1, this.getWidth()-2, this.getHeight()-2);
	    
	    graphics.dispose();
	
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		this.repaint();
	}
}
